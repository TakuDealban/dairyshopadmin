<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
    app()['cache']->forget('spatie.permission.cache');

    
    Role::create(['name' => 'User']);
    /** @var \App\User $user */
    $user = factory(\App\User::class)->create(
        [
            'role' => 1,
        ]
    );
    $user->assignRole('User');
   
    Role::create(['name' => 'Admin']);
    Role::create(['name' => 'SA']);

    /** @var \App\User $user */
    $admin = factory(\App\User::class)->create([
        'name' => 'Admin Admin',
        'email' => 'admin@dairy.com',
        'role' => 3,
        'email_verified_at' => now(),
        'password' => Hash::make('12345'),
        'created_at' => now(),
        'updated_at' => now()
    ]);

    $admin->assignRole('SA');
    }
}
