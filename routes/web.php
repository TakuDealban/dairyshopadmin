<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  //  return redirect()->route('login');
    return view('auth.login');
});


Auth::routes();

Route::middleware('auth')->group(function (){
  if (App::environment() == "remote"){
    Route::get('/dashboard',['uses' => 'ProductController@upload_product_view', 'as' => 'import_products']);
  }else{
    Route::get('/dashboard', 'ReportingController@index')->name('dash');
  }

  //Route::get('/dashboard', 'ProductController@index')->name('dash');

  //categories
  Route::get('/categories',['uses' => 'CategoryController@index','as' => 'categories']);
  Route::get('/new-category',['uses' => 'CategoryController@create','as' => 'newcategory']);
  Route::post('/save-category',['uses' => 'CategoryController@store','as' => 'savecategory']);
  Route::get('/edit-category/{cat_id}',['uses' => 'CategoryController@edit','as' => 'editcategory']);
  Route::post('/update-category/{cat_id}/category',['uses' => 'CategoryController@update','as' => 'updatecategory']);

   //prod
   Route::get('/products',['uses' => 'ProductController@index','as' => 'products']);
   Route::get('/new-product',['uses' => 'ProductController@create','as' => 'newproduct']);
   Route::post('/save-product',['uses' => 'ProductController@store','as' => 'saveproduct']);
   Route::get('/edit-product/{prod_id}',['uses' => 'ProductController@edit','as' => 'editproduct']);
   Route::post('/update-product/{prod_id}/product',['uses' => 'ProductController@update','as' => 'updateproduct']);
   Route::get('/import_products',['uses' => 'ProductController@upload_product_view', 'as' => 'import_products']);
   Route::get('/import_from_file',['uses' => 'ProductController@import_from_excel_product_view', 'as' => 'import_from_file']);
    Route::post('/process_products_pricelist',['uses' => 'ProductController@load_price_lists', 'as' => 'load_price_lists']);
    //rates
    Route::get('/exchange_rates',['uses' => 'ExchangeRateController@index','as' => 'exchangerates']);
    Route::get('/new-exchangerate',['uses' => 'ExchangeRateController@create','as' => 'newexchangerate']);
    Route::post('/save-exchangerate',['uses' => 'ExchangeRateController@store','as' => 'saveexchangerate']);
    Route::get('/edit-exchange/{id}',['uses' => 'ExchangeRateController@edit','as' => 'editexchangerate']);
    Route::post('/update-exchangerate/{id}',['uses' => 'ExchangeRateController@update','as' => 'updateexchangerate']);

     //shippmentplans
     Route::get('/shippingplans',['uses' => 'ShippingPlanController@index','as' => 'shippingplans']);
     Route::get('/new-plan',['uses' => 'ShippingPlanController@create','as' => 'newplan']);
     Route::post('/save-plan',['uses' => 'ShippingPlanController@store','as' => 'saveplan']);
     Route::get('/edit-plan/{plan_id}',['uses' => 'ShippingPlanController@edit','as' => 'editplan']);
     Route::post('/update-plan/{plan_id}',['uses' => 'ShippingPlanController@update','as' => 'updateplan']);

     //company
     Route::get('/company',['uses' => 'CompanyController@index','as' => 'company']);
     Route::post('/updatedemodata',['uses' => 'CompanyController@updateCompanyDemographics','as' => 'updatedemodata']); //updateCompanyLogo
     Route::post('/updatecompanylogo',['uses' => 'CompanyController@updateCompanyLogo','as' => 'updatecompanylogo']);
     Route::post('/updatePaynow',['uses' => 'CompanyController@updatePaynow','as' => 'updatePaynow']);

     Route::get('/transactions', ['uses' => 'TransactionController@showTransactions','as' => 'showTransactions']);
     Route::get('/attempts', ['uses' => 'TransactionController@showAttewmpts','as' => 'showAttewmpts']);
     Route::get('/customers', ['uses' => 'TransactionController@Customers','as' => 'Customers']);
     Route::get('/update_invoice/{inv_number}', ['uses' => 'TransactionController@viewUpdateInvoiceStatus','as' => 'viewUpdateInvoiceStatus']);
     Route::post('/updateinvoice', ['uses' => 'TransactionController@processInvoiceStatus','as' => 'processInvoiceStatus']);
     Route::resource('user', 'UserController');
     Route::get('/transaction/{ref}', ['uses' => 'TransactionController@TransactionDetails','as' => 'TransactionDetails']);
     Route::get('/push-to-sap', ['uses' => 'TransactionController@invoicesToPush','as' => 'pushToSap']);
     //Route::get('/sms_balance', ['uses' => 'TransactionController@ShowBalance','as' => 'getSMSBalance']);

     //sap integrations
    // Route::get('/cart_materials', ['uses' => 'SapIntegrationController@show_materials']);
     Route::post('/materials_upload', ['uses' => 'SapIntegrationController@get_all_materials_with_details', 'as' =>  'materials_upload']);
     Route::get('/UploadPrices', ['uses' => 'SapIntegrationController@cart_price_list']);
     Route::get('/sync_invoices',['uses' => 'SapInvoicingController@syncInvoicesToSap', 'as' => 'sync_invoices']);
     Route::get('/webservices_configs',['uses' => 'SAPWebServicesController@index', 'as' => 'sapweb']);
     Route::get('/pick_up_points',['uses' => 'SAPWebServicesController@newpickup', 'as' => 'newpickup']);
     Route::post('/updatewebservice',['uses' => 'SAPWebServicesController@saveWebServiceSettings','as' => 'updatewebservice']);
     Route::post('/createpickups',['uses' => 'SAPWebServicesController@savePickUpPointsSettings','as' => 'createpickups']);
     Route::post('/updatepickups/{id}',['uses' => 'SAPWebServicesController@updatePickUpPointsSettings','as' => 'updatepickups']);
     Route::get('/editpoint/{id}',['uses' => 'SAPWebServicesController@edit','as' => 'editpoint']);
     Route::get('/storage_locations',['uses' => 'SAPWebServicesController@vwStorageLocations', 'as' => 'vwStorageLocations']);
     Route::get('/sloc_products/{sloc}',['uses' => 'SAPWebServicesController@vwStorageLocProducts', 'as' => 'vwStorageLocProducts']);
     Route::get('/upload_sap_register',['uses' => 'SapInvoicingController@import_from_excel_invoice_view', 'as' => 'upload_sap_register']);
     Route::post('/process_sap_register',['uses' => 'SapInvoicingController@load_billing_documents', 'as' => 'process_sap_register']);
     //editpoint

   //  Route::get('/sales_offices_pricing', ['uses' => 'SapIntegrationController@sales_office_price_list']);
   
   //exceptions
   Route::get('exceptions/receipts',['uses' => 'ExceptionsController@ListFailedTransactions', 'as' => 'failed_trans']);
   Route::get('exceptions/receipts/{order_ref}',['uses' => 'ExceptionsController@FixFailedTrans', 'as' => 'fix_trans']);

   //reports
   Route::get('/usersList', 'ReportingController@index')->name('usersList');


});

Route::middleware('auth')->get('logout', function() {
    Auth::logout();
    return redirect(route('login'))->withInfo('You have successfully logged out!');
})->name('logout');

Route::get('site/shutdown', function(){
  return Artisan::call('down');
});

Route::get('site/live', function(){
  return Artisan::call('up');
}); 
