 
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h4 class="title nk-block-title">Request Product From ERP</h4> 
                                        <div class="nk-block-des">
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                            @elseif(session('error'))
                                            <br>
                                            <p class="alert alert-fill alert-danger alert-icon">{{ session('error') }}</p>
                                         
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <div class="preview-block">
                                            {{-- <span class="preview-title-lg overline-title">Default Preview</span> --}}
                                        <form action="{{route('materials_upload')}}" method="POST" enctype="multipart/form-data">
                                          @csrf
                                            <div class="row gy-4">
                                               

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label">Storage Location </label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select form-control-lg @error('storage_location') is-invalid @enderror" name="storage_location" id="storage_location" data-ui="lg" data-search="on" required>
                                                               <option value="">Please Select Storage Location</option>
                                                                @foreach ($pick_up_points as $item)
                                                            <option value="{{$item->storage_location}}" data-pick_up_point="{{$item->pick_up_point}}" data-plant="{{$item->plant}}">{{$item->storage_location}} - {{$item->pick_up_point}}</option>
                                                                @endforeach
                                                               
                                                            </select>

                                                            @error('storage_location')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                               
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Plant</label>
                                                        <div class="form-control-wrap">
                                                            
                                                        <input type="text" name="plant" class="form-control  form-control-lg @error('plant') is-invalid @enderror plant" value="{{old('plant')}}" id="plant" readonly>
                                                            @error('plant')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Pick Up Point</label>
                                                        <div class="form-control-wrap">
                                                            
                                                        <input type="text" name="pick_up_point" class="form-control  form-control-lg @error('pick_up_point') is-invalid @enderror pick_up_point" value="{{old('pick_up_point')}}" id="pick_up_point" readonly>
                                                            @error('pick_up_point')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                               

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-lg btn-primary">Upload Products</button>
                                                    </div>
                                                
                                                </div>
                                                
                                            </div>
                                        </form>
                                            
                                        </div>
                                    </div>
                                </div><!-- .card-preview -->
                                
                            </div><!-- .nk-block -->
                           
                            
                            
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section('js')
    <script> 
    $(document).ready(function(){
        const format = (num, decimals) => num.toLocaleString('en-US', {
   minimumFractionDigits: 2,      
   maximumFractionDigits: 2,
        });

        $("#storage_location").on('change',function(){
            
           var plant =  $(this).find(':selected').attr('data-plant'); 
           var pick_up_point = $(this).find(':selected').attr('data-pick_up_point'); 

           $("#pick_up_point").val(pick_up_point);
           $("#plant").val(plant);
          
        });
       


    });

    </script>
            
        @endsection