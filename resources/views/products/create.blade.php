 
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h4 class="title nk-block-title">New Product</h4> 
                                        <div class="nk-block-des">
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <div class="preview-block">
                                            {{-- <span class="preview-title-lg overline-title">Default Preview</span> --}}
                                        <form action="{{route('saveproduct')}}" method="POST" enctype="multipart/form-data">
                                          @csrf
                                            <div class="row gy-4">
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-01">Product Name</label>
                                                        <div class="form-control-wrap">
                                                        <input type="text" name="name" class="form-control form-control-lg @error('name') is-invalid @enderror" value="{{old('name')}}" id="default-01" placeholder="Enter Product Name" required>
                                                            @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Product Code</label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">code</span>
                                                            </div>
                                                        <input type="text" name="product_code" class="form-control  form-control-lg @error('product_code') is-invalid @enderror" value="{{old('product_code')}}" id="default-05" placeholder="Enter product code">
                                                            @error('product_code')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label">Product TAX Code </label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select form-control-lg @error('VAT_CODE') is-invalid @enderror" name="VAT_CODE" data-ui="lg" data-search="on" required>
                                                            <option value="A">VAT CODE A = {{config('app.ZIMRA_VAT_CODE')}}</option>
                                                            <option value="B">VAT CODE B = 0</option>
                                                            </select>

                                                            @error('VAT_CODE')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label">Product Category </label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select form-control-lg @error('category_id') is-invalid @enderror" name="category_id" data-ui="lg" data-search="on" required>
                                                               @foreach ($categories as $category)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                               @endforeach
                                                            </select>

                                                            @error('category_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Product Selling Price</label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">{{config('app.BASE_CURRENCY')}} </span>
                                                            </div>
                                                        <input type="number" min="0" step="any" name="price" class="form-control  form-control-lg @error('price') is-invalid @enderror unit_price" value="{{old('price')}}" id="default-05" placeholder="Enter  product price">
                                                            @error('price')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Product Case Structure</label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">Case <em class="text-info">6 : 1 case = 6 Items</em></span>
                                                            </div>
                                                        <input type="number" min="0"  name="case_value" class="form-control  form-control-lg @error('case_value') is-invalid @enderror  case_value" value="{{old('case_value')}} " id="default-05" placeholder="Enter  product Case structure">
                                                            @error('case_value')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Product Case Price</label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">{{config('app.BASE_CURRENCY')}} </span>
                                                            </div>
                                                        <input type="number" min="0" step="any" name="case_price" class="form-control  form-control-lg @error('case_price') is-invalid @enderror  case_price" value="{{old('case_price')}}" id="default-05" placeholder="Enter  product Case structure">
                                                            @error('case_price')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Product Take On Quanity</label>
                                                        <div class="form-control-wrap">
                                                            
                                                        <input type="number" name="stock_on_hand" class="form-control  form-control-lg @error('stock_on_hand') is-invalid @enderror" value="{{old('stock_on_hand')}}" id="default-05" placeholder="Enter  product take on quantity">
                                                            @error('stock_on_hand')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-06">Product Image Filter</label>
                                                        <div class="form-control-wrap">
                                                            <div class="custom-file">
                                                                <input type="file" name="image" class="custom-file-input  custom-file-input-lg  @error('image') is-invalid @enderror" id="customFile">
                                                                <label class="custom-file-label " for="customFile">Choose file</label>
                                                                @error('image')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-lg btn-primary">Save Product</button>
                                                    </div>
                                                
                                                </div>
                                                
                                            </div>
                                        </form>
                                            
                                        </div>
                                    </div>
                                </div><!-- .card-preview -->
                                
                            </div><!-- .nk-block -->
                           
                            
                            
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section('js')
    <script> 
    $(document).ready(function(){
        const format = (num, decimals) => num.toLocaleString('en-US', {
   minimumFractionDigits: 2,      
   maximumFractionDigits: 2,
});

       $(".case_value").on('keyup',function(){
           var unit_price = parseFloat($(".unit_price").val());
           var case_total = 0;
           if(unit_price > 0){
            case_total = unit_price * $(this).val();
           }
          
           $(".case_price").val(case_total.toFixed(2))
       })
    });

    </script>
            
        @endsection