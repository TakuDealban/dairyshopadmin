 <!-- sidebar @s -->
 <div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="nk-sidebar-brand">
            <a href="/dashboard" class="logo-link">
                <img class="logo-light logo-img" src="/uploads/company/logo.png" srcset="/uploads/company/logo.png 2x" alt="logo">
                    <img class="logo-dark logo-img" src="/uploads/company/logo.png" srcset="/uploads/company/logo.png 2x" alt="logo-dark">
                    <span class="nio-version"></span>
            </a>
        </div>
        <div class="nk-menu-trigger mr-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
        </div>

        <?php 
        $user_role = "";
          foreach (Auth()->user()->getRoleNames() as $role){
              $user_role = $role;
          }

          

        ?>

    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-content">
            <div class="nk-sidebar-menu" data-simplebar>
                @if (App::environment() == "remote")
                <ul class="nk-menu">
                <li class="nk-menu-heading">
                    <h6 class="overline-title text-primary-alt">SAP Integrations Panel</h6>
                </li><!-- .nk-menu-heading -->
              
                <li class="nk-menu-item">
                    <a href="{{route('import_products')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-swap-alt"></em></span>
                            <span class="nk-menu-text">Download Products</span>
                        </a>
                    </li><!-- .nk-menu-item -->


                        <li class="nk-menu-item">
                            <a href="{{route('pushToSap')}}" class="nk-menu-link">
                                    <span class="nk-menu-icon"><em class="icon ni ni-growth"></em></span>
                                    <span class="nk-menu-text">Upload Invoices</span>
                                </a>
                            </li><!-- .nk-menu-item -->
                        </ul><!-- .nk-menu -->
                @else
                
                <ul class="nk-menu">
                    <li class="nk-menu-heading">
                    <h6 class="overline-title text-primary-alt">Dashboard Panel </h6>
                    </li><!-- .nk-menu-item -->
                    {{-- <li class="nk-menu-item">
                    <a href="{{route('dash')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-dashlite"></em></span>
                            <span class="nk-menu-text">Home</span>
                        </a>
                    </li><!-- .nk-menu-item --> --}}
                    <li class="nk-menu-item">
                        <a href="{{route('dash')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-growth"></em></span>
                            <span class="nk-menu-text">Dashboard</span>
                        </a>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">Dairy Delights Panel</h6>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item">
                        <a href="{{route('showTransactions')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-coins"></em></span>
                            <span class="nk-menu-text">Transactions</span>
                            {{-- <span class="nk-menu-badge">3</span> --}}
                        </a>
                    </li><!-- .nk-menu-item -->
                    {{-- <li class="nk-menu-item">
                    <a href="{{route('showAttewmpts')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-coins"></em></span>
                            <span class="nk-menu-text">Attempts</span>
                            {{-- <span class="nk-menu-badge">2</span> --}}
                        {{-- </a> --}}
                    {{-- </li><!-- .nk-menu-item --> --}} 

                    <li class="nk-menu-item">
                    <a href="{{route('Customers')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-users-fill"></em></span>
                            <span class="nk-menu-text">Customers</span>
                            {{-- <span class="nk-menu-badge">2</span> --}}
                        </a>
                    </li><!-- .nk-menu-item -->

                    {{-- <li class="nk-menu-item">
                    <a href="#" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-share-alt"></em></span>
                            <span class="nk-menu-text">Promotions</span>
                            {{-- <span class="nk-menu-badge">4</span> --}}
                        {{-- </a> --}}
                    {{-- </li><!-- .nk-menu-item --> --}} 

                    <li class="nk-menu-item">
                        <a href="{{route('failed_trans')}}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-share-alt"></em></span>
                                <span class="nk-menu-text">Exceptions</span>
                                {{-- <span class="nk-menu-badge">4</span> --}}
                            </a>
                        </li><!-- .nk-menu-item -->

                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">Master Data Panel</h6>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item">
                    <a href="{{route('categories')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-cards-fill"></em></span>
                            <span class="nk-menu-text">Product Categories</span>
                        </a>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item">
                    <a href="{{route('products')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-calendar-booking"></em></span>
                            <span class="nk-menu-text">Products Master</span>
                        </a>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item">
                        <a href="{{route('vwStorageLocations')}}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-building-fill"></em></span>
                                <span class="nk-menu-text">Storage Locations</span>
                            </a>
                        </li><!-- .nk-menu-item -->
                    
                       
                    @if($user_role == "Super Admin")
                    <li class="nk-menu-item">
                    <a href="{{route('shippingplans')}}" class="nk-menu-link" >
                            <span class="nk-menu-icon"><em class="icon ni ni-truck"></em></span>
                            <span class="nk-menu-text">Shipping Plans</span>
                        </a>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item">
                    <a href="{{route('exchangerates')}}" class="nk-menu-link" >
                            <span class="nk-menu-icon"><em class="icon ni ni-invest"></em></span>
                            <span class="nk-menu-text">Exchange Rates</span>
                        </a>
                    </li><!-- .nk-menu-item -->
                    @endif

                    @if($user_role == "Super Admin")
                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">Dairibord Company Panel</h6>
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item">
                    <a href="{{route('company')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-setting-alt-fill"></em></span>
                            <span class="nk-menu-text">Company Information</span>
                        </a>
                    </li><!-- .nk-menu-item -->
                    

                    <li class="nk-menu-item">
                        <a href="{{route('company')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-paypal-alt"></em></span>
                            <span class="nk-menu-text">Billing Keys </span>
                        </a>
                    </li><!-- .nk-menu-item -->

                    <li class="nk-menu-item has-sub">
                        <a href="#" class="nk-menu-link nk-menu-toggle">
                            <span class="nk-menu-icon"><em class="icon ni ni-users"></em></span>
                            <span class="nk-menu-text">User Manage</span>
                        </a>
                        <ul class="nk-menu-sub">
                            <li class="nk-menu-item">
                            <a href="{{route('user.create')}}" class="nk-menu-link"><span class="nk-menu-text">New User</span></a>
                            </li>
                            <li class="nk-menu-item">
                            <a href="{{route('user.index')}}" class="nk-menu-link"><span class="nk-menu-text">User List</span></a>
                            </li>
                            
                        </ul><!-- .nk-menu-sub -->
                    </li><!-- .nk-menu-item -->
                 
                    <li class="nk-menu-heading">
                        <h6 class="overline-title text-primary-alt">SAP Integrations Panel</h6>
                    </li><!-- .nk-menu-heading -->
                    <li class="nk-menu-item">
                    <a href="{{route('sapweb')}}" class="nk-menu-link">
                            <span class="nk-menu-icon"><em class="icon ni ni-laravel"></em></span>
                            <span class="nk-menu-text">Configure SAP Rules</span>
                        </a>
                    </li><!-- .nk-menu-item -->
                    <li class="nk-menu-item">
                        <a href="{{route('import_products')}}" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-swap-alt"></em></span>
                                <span class="nk-menu-text">Upload Products</span>
                            </a>
                        </li><!-- .nk-menu-item -->

                        <li class="nk-menu-item">
                            <a href="{{route('import_from_file')}}" class="nk-menu-link">
                                    <span class="nk-menu-icon"><em class="icon ni ni-swap-alt"></em></span>
                                    <span class="nk-menu-text">Import Products</span>
                                </a>
                            </li><!-- .nk-menu-item -->

                            <li class="nk-menu-item">
                                <a href="{{route('upload_sap_register')}}" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-swap-alt"></em></span>
                                        <span class="nk-menu-text">Import Billing Documents</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                    @endif
                    
                 
          
                   
                </ul><!-- .nk-menu -->
                    
                @endif
            </div><!-- .nk-sidebar-menu -->
        </div><!-- .nk-sidebar-content -->
    </div><!-- .nk-sidebar-element -->
</div>
<!-- sidebar @e -->