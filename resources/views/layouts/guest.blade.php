<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Login') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    {{-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> --}}
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}
    <link rel="shortcut icon" href="{{asset('theme')}}/images/favicon.png">
    
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="{{ asset('theme') }}/css/dashlite.css?ver=1.4.0">
    <link id="skin-default" rel="stylesheet" href="{{ asset('theme') }}/css/theme.css?ver=1.4.0">
</head>
<body>
    @yield('content')
</body>


<script src="{{ asset('theme') }}/js/bundle.js?ver=1.4.0"></script>
<script src="{{ asset('theme') }}/js/scripts.js?ver=1.4.0"></script>
</html>

