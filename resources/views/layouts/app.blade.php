<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'DairyDelights') }}</title>


    <link rel="shortcut icon" href="{{asset('theme')}}/images/favicon.png">
    
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="{{ asset('theme') }}/css/dashlite.css?ver=1.4.0">
    <link id="skin-default" rel="stylesheet" href="{{ asset('theme') }}/css/theme.css?ver=1.4.0">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
</head>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
       @include('layouts.sidebar.sidebar')
       <div class="nk-wrap ">
           @include('layouts.navbar.navbar')
            @yield('content')
            @include('layouts.footer.footer')
       </div>
        
    </div>
    <!-- main @e -->
</div>
<!-- app-root @e -->
</body>

<script src="{{ asset('theme') }}/js/bundle.js?ver=1.4.0"></script>
<script src="{{ asset('theme') }}/js/scripts.js?ver=1.4.0"></script>
@yield('js')
</html>
