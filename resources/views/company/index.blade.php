
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                        <div class="col-md-9">
                                            <h4 class="nk-block-title">Company Master </h4>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                       
                                        <div class="nk-block-des col-md-3">
                                            {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
                                        {{-- <a class="btn btn-primary pull-right" href="{{route('newcategory')}}">Companu</a> --}}
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#tabItem5"><em class="icon ni ni-setting-alt-fill"></em><span>Company Details</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#tabItem8"><em class="icon ni  ni-focus"></em><span>Crop Company Image</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#tabItem6"><em class="icon ni  ni-cc-visa"></em><span>Paynow Information</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#tabItem7"><em class="icon ni ni-paypal-alt"></em><span>Paypal Information</span></a>
                                            </li>
                                            
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tabItem5">
                                                <form action="{{route('updatedemodata')}}" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                      <div class="row gy-4">
                                                          <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <label class="form-label" for="default-01">Company Name</label>
                                                                  <div class="form-control-wrap">
                                                                  <input type="text" name="Name" class="form-control form-control-lg @error('Name') is-invalid @enderror" value="{{$company->Name}}" id="default-01" placeholder="Enter Company Name" required>
                                                                      @error('Name')
                                                                      <span class="invalid-feedback" role="alert">
                                                                          <strong>{{ $message }}</strong>
                                                                      </span>
                                                                  @enderror
                                                                  </div>
                                                              </div>
                                                          </div>

                                                          <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Company Email</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        <span class="overline-title">Contact Email</span>
                                                                    </div>
                                                                <input type="text" name="contact_email" class="form-control  form-control-lg @error('contact_email') is-invalid @enderror" value="{{$company->contact_email}}" id="default-05" placeholder="Enter Company Address (one)" required>
                                                                    @error('contact_email')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                          <div class="form-group">
                                                              <label class="form-label" for="default-05">Company Telephone</label>
                                                              <div class="form-control-wrap">
                                                                  <div class="form-text-hint">
                                                                      <span class="overline-title">Company Phone</span>
                                                                  </div>
                                                              <input type="text" name="contact_tel" class="form-control  form-control-lg @error('contact_tel') is-invalid @enderror" value="{{$company->contact_tel}}" id="default-05" placeholder="Enter Company Address (two)" required>
                                                                  @error('contact_tel')
                                                                  <span class="invalid-feedback" role="alert">
                                                                      <strong>{{ $message }}</strong>
                                                                  </span>
                                                              @enderror
                                                              </div>
                                                          </div>
                                                      </div>
                                                         

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Company Business Partner Number</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        
                                                                    </div>
                                                                <input type="text" name="BPN" class="form-control  form-control-lg @error('BPN') is-invalid @enderror" value="{{$company->BPN}}" id="default-05" placeholder="Enter Company Address (one)" required>
                                                                    @error('BPN')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Company VAT</label>
                                                                <div class="form-control-wrap">
                                                                 
                                                                <input type="text" name="VATNo" class="form-control  form-control-lg @error('VATNo') is-invalid @enderror" value="{{$company->VATNo}}" id="default-05" placeholder="Enter Company VATNo" required>
                                                                    @error('VATNo')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Company Tax Bill</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                    <span class="overline-title">{{config('app.ZIMRA_VAT_CODE')}}</span>
                                                                    </div>
                                                                <input type="text" name="Vat_Rate" class="form-control  form-control-lg @error('Vat_Rate') is-invalid @enderror" value="{{$company->Vat_Rate}}" id="default-05" placeholder="Enter Company Vat_Rate" required>
                                                                    @error('Vat_Rate')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Company Base Currency</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        <span class="overline-title">{{config('app.BASE_CURRENCY')}}</span>
                                                                        </div>
                                                                <input type="text" name="BaseCurrency" class="form-control  form-control-lg @error('BaseCurrency') is-invalid @enderror" value="{{$company->BaseCurrency}}" id="default-05" placeholder="Enter Company BaseCurrency" required>
                                                                    @error('BaseCurrency')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Minimum Order Value</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        <span class="overline-title">{{config('app.BASE_CURRENCY')}}</span>
                                                                        </div>
                                                                <input type="number" name="MinOrderValue" class="form-control  form-control-lg @error('MinOrderValue') is-invalid @enderror" value="{{$company->minimum_order_value}}" id="default-05" placeholder="Enter Company MinOrderValue" required>
                                                                    @error('MinOrderValue')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Company Address One</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        <span class="overline-title">Number 2</span>
                                                                    </div>
                                                                <input type="text" name="addr_one" class="form-control  form-control-lg @error('addr_one') is-invalid @enderror" value="{{$company->addr_one}}" id="default-05" placeholder="Enter Company Address (one)" required>
                                                                    @error('addr_one')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                          <div class="form-group">
                                                              <label class="form-label" for="default-05">Company Address Two</label>
                                                              <div class="form-control-wrap">
                                                                  <div class="form-text-hint">
                                                                      <span class="overline-title">Rekayi Tangwena Avenue</span>
                                                                  </div>
                                                              <input type="text" name="addr_two" class="form-control  form-control-lg @error('addr_two') is-invalid @enderror" value="{{$company->addr_two}}" id="default-05" placeholder="Enter Company Address (two)" required>
                                                                  @error('addr_two')
                                                                  <span class="invalid-feedback" role="alert">
                                                                      <strong>{{ $message }}</strong>
                                                                  </span>
                                                              @enderror
                                                              </div>
                                                          </div>
                                                      </div>
                                                      

                                                      <div class="col-sm-6">
                                                          <div class="form-group">
                                                              <label class="form-label" for="default-05">Company Address Three</label>
                                                              <div class="form-control-wrap">
                                                                  <div class="form-text-hint">
                                                                      <span class="overline-title">Harare, Zimbabwe</span>
                                                                  </div>
                                                              <input type="text" name="addr_three" class="form-control  form-control-lg @error('addr_three') is-invalid @enderror" value="{{$company->addr_three}}" id="default-05" placeholder="Enter Company Address (one)" required>
                                                                  @error('addr_three')
                                                                  <span class="invalid-feedback" role="alert">
                                                                      <strong>{{ $message }}</strong>
                                                                  </span>
                                                              @enderror
                                                              </div>
                                                          </div>
                                                      </div>
                                                        
                                                          
                                                          
                                                          
          
                                                          <div class="col-sm-8">
                                                              <div class="form-group">
                                                                  <button type="submit" class="btn btn-lg btn-primary">Update Company Data</button>
                                                              </div>
                                                          
                                                          </div>
                                                          
                                                      </div>
                                                  </form>
                                            </div>
                                            <div class="tab-pane" id="tabItem8">
                                                <form action="{{route('updatecompanylogo')}}" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                      <div class="row gy-4">

                                                        <div class="col-sm-8">
                                                            <img src="{{$company->company_image_path}}/{{$company->logo}}" alt="" width="300px" height="250px">
                                                        </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-06">Company Image Filter</label>
                                                        <div class="form-control-wrap">
                                                            <div class="custom-file">
                                                                <input type="file" name="image" class="custom-file-input  custom-file-input-lg  @error('image') is-invalid @enderror" id="customFile">
                                                                <label class="custom-file-label " for="customFile">Choose file</label>
                                                                @error('image')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-lg btn-primary">Update Company Logo</button>
                                                    </div>
                                                
                                                </div>

                                               
                                                      </div>
                                                </form>

                                            </div>
                                            <div class="tab-pane" id="tabItem6">
                                            <form action="{{route('updatePaynow')}}" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                      <div class="row gy-4">
                                                          <div class="col-sm-8">
                                                              <div class="form-group">
                                                                  <label class="form-label" for="default-01">Company Integration Key</label>
                                                                  <div class="form-control-wrap">
                                                                  <input type="text" name="Paynow_Integration_Key" class="form-control form-control-lg @error('Paynow_Integration_Key') is-invalid @enderror" value="{{$company->Paynow_Integration_Key}}" id="default-01" placeholder="Enter Company Paynow_Integration_Key" required>
                                                                      @error('Paynow_Integration_Key')
                                                                      <span class="invalid-feedback" role="alert">
                                                                          <strong>{{ $message }}</strong>
                                                                      </span>
                                                                  @enderror
                                                                  </div>
                                                              </div>
                                                          </div>

                                                         

                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Company Integration ID</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        
                                                                    </div>
                                                                <input type="text" name="Paynow_Integration_id" class="form-control  form-control-lg @error('Paynow_Integration_id') is-invalid @enderror" value="{{$company->Paynow_Integration_id}}" id="default-05" placeholder="Enter Paynow_Integration_id" required>
                                                                    @error('Paynow_Integration_id')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>
          
                                                          <div class="col-sm-8">
                                                              <div class="form-group">
                                                                  <button type="submit" class="btn btn-lg btn-primary">Update Integration Details</button>
                                                              </div>
                                                          
                                                          </div>
                                                          
                                                      </div>
                                                  </form>
                                            </div>
                                            <div class="tab-pane" id="tabItem7">
                                                <form action="" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                      <div class="row gy-4">
                                                          <div class="col-sm-8">
                                                              <div class="form-group">
                                                                  <label class="form-label" for="default-01">Company Integration Key</label>
                                                                  <div class="form-control-wrap">
                                                                  <input type="text" name="Paynow_Integration_Key" class="form-control form-control-lg @error('Paynow_Integration_Key') is-invalid @enderror" value="{{$company->Paynow_Integration_Key}}" id="default-01" placeholder="Enter Company Paynow_Integration_Key" required>
                                                                      @error('Paynow_Integration_Key')
                                                                      <span class="invalid-feedback" role="alert">
                                                                          <strong>{{ $message }}</strong>
                                                                      </span>
                                                                  @enderror
                                                                  </div>
                                                              </div>
                                                          </div>

                                                         

                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Company Integration ID</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        
                                                                    </div>
                                                                <input type="text" name="Paynow_Integration_id" class="form-control  form-control-lg @error('Paynow_Integration_id') is-invalid @enderror" value="{{$company->Paynow_Integration_id}}" id="default-05" placeholder="Enter Company Address (one)">
                                                                    @error('Paynow_Integration_id')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>
          
                                                          <div class="col-sm-8">
                                                              <div class="form-group">
                                                                  <button type="submit" class="btn btn-lg btn-primary">Update Integration Details</button>
                                                              </div>
                                                          
                                                          </div>
                                                          
                                                      </div>
                                                  </form>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection