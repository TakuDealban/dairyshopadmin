
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                        <div class="col-md-9">
                                            <h4 class="nk-block-title">Categories </h4>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                       
                                        <div class="nk-block-des col-md-3">
                                            {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
                                        <a class="btn btn-primary pull-right" href="{{route('newcategory')}}">New Category</a>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                            <thead>
                                                <tr class="nk-tb-item nk-tb-head">
                                                    {{-- <th class="nk-tb-col nk-tb-col-check">
                                                        <div class="custom-control custom-control-sm custom-checkbox notext">
                                                            <input type="checkbox" class="custom-control-input" id="uid">
                                                            <label class="custom-control-label" for="uid"></label>
                                                        </div>
                                                    </th> --}}
                                                    <th class="nk-tb-col"><span class="sub-text">Category Image</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Category Name</span></th>
                                                    <th class="nk-tb-col tb-col-md"><span class="sub-text">Category Description</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Number of Products</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Created Date</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text"></span></th>
                                                    {{-- <th class="nk-tb-col nk-tb-col-tools text-right">
                                                        <div class="dropdown">
                                                            <a href="#" class="btn btn-xs btn-outline-light btn-icon dropdown-toggle" data-toggle="dropdown" data-offset="0,5"><em class="icon ni ni-plus"></em></a>
                                                            <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                                                <ul class="link-tidy sm no-bdr">
                                                                    <li>
                                                                        <div class="custom-control custom-control-sm custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" checked="" id="bl">
                                                                            <label class="custom-control-label" for="bl">Balance</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-control-sm custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" checked="" id="ph">
                                                                            <label class="custom-control-label" for="ph">Phone</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-control-sm custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="vri">
                                                                            <label class="custom-control-label" for="vri">Verified</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-control-sm custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="st">
                                                                            <label class="custom-control-label" for="st">Status</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($categories as $category)
                                                <tr class="nk-tb-item">
                                                    {{-- <td class="nk-tb-col nk-tb-col-check">
                                                        <div class="custom-control custom-control-sm custom-checkbox notext">
                                                            <input type="checkbox" class="custom-control-input" id="uid1">
                                                            <label class="custom-control-label" for="uid1"></label>
                                                        </div>
                                                    </td> --}}
                                                    <td class="nk-tb-col">
                                                        <div class="user-card">
                                                            <div class="user-avatar bg-dim-primary d-none d-sm-flex">
                                                                <span><img src="/uploads/categories/{{$category->image}}" alt=""></span>
                                                            </div>
                                                         
                                                        </div>
                                                    </td>
                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$category->name}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                           
                                                        </div>
                                                    </td>
                                                    <td class="nk-tb-col tb-col-md">
                                                        <span>{{$category->description}}</span>
                                                    </td>
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <ul class="list-status">
                                                        <li><em class="icon text-success ni ni-check-circle"></em> <span>{{count($category->cat_products)}}</span></li>
                                                            
                                                        </ul>
                                                    </td>
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{$category->created_at->format('d M y')}}</span>
                                                    </td>
                                                   
                                                    <td class="nk-tb-col nk-tb-col-tools">
                                                        <ul class="nk-tb-actions gx-1">
                                                            {{-- <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Wallet">
                                                                    <em class="icon ni ni-wallet-fill"></em>
                                                                </a>
                                                            </li>
                                                            <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Send Email">
                                                                    <em class="icon ni ni-mail-fill"></em>
                                                                </a>
                                                            </li>
                                                            <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Suspend">
                                                                    <em class="icon ni ni-user-cross-fill"></em>
                                                                </a>
                                                            </li> --}}
                                                            <li>
                                                                <div class="drodown">
                                                                    <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        <ul class="link-list-opt no-bdr">
                                                                        <li><a href="{{route('editcategory',$category->id)}}"><em class="icon ni ni-focus"></em><span>Edit Category</span></a></li>
                                                                            <li><a href="#"><em class="icon ni ni-focus"></em><span>Quick View</span></a></li>
                                                                            <li><a href="#"><em class="icon ni ni-eye"></em><span>View Logs</span></a></li>
                                                                            <li><a href="#"><em class="icon ni ni-eye"></em><span>View Products</span></a></li>
                                                                         
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr><!-- .nk-tb-item  -->
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection