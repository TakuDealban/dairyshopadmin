 
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h4 class="title nk-block-title">New User</h4> 
                                        <div class="nk-block-des">
                                            {{-- <p class="alert alert-fill alert-info alert-icon"> <strong> {{ config('app.BASE_CURRENCY') }} Paired Currency combinations </strong> : {{ $currencies }}</p> --}}
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <div class="preview-block">
                                            {{-- <span class="preview-title-lg overline-title">Default Preview</span> --}}
                                        <form action="{{route('user.store')}}" method="POST" enctype="multipart/form-data">
                                          @csrf
                                            <div class="row gy-4">
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-01">Name</label>
                                                        <div class="form-control-wrap">
                                                        <input type="text" name="name" class="form-control form-control-lg @error('name') is-invalid @enderror" value="{{old('name')}}" id="currency_code" placeholder="Enter  Name " required>
                                                            @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Email Address</label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">user@user.com</span>
                                                            </div>
                                                        <input type="email" name="email" class="form-control  form-control-lg @error('email') is-invalid @enderror" value="{{old('email')}}" id="default-05" placeholder="Enter user email">
                                                            @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label">User Role</label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select form-control-lg @error('role') is-invalid @enderror" name="role" data-ui="lg" data-search="on" required>
                                                               @foreach ($roles as $role)
                                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                                               @endforeach
                                                            </select>

                                                            @error('role')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">User Password</P></label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">Secure Key</span>
                                                            </div>
                                                        <input type="password" name="password" class="form-control  form-control-lg @error('password') is-invalid @enderror" value="{{old('password')}}" id="default-05" placeholder="Enter User Default Password">
                                                            @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Confirm Password</P></label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">Secure Key</span>
                                                            </div>
                                                        <input type="password" name="cpassword" class="form-control  form-control-lg @error('cpassword') is-invalid @enderror" value="{{old('cpassword')}}" id="default-05" placeholder="Re Enter password to confirm">
                                                            @error('cpassword')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                              

                                            <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-lg btn-primary">Save User</button>
                                                    </div>
                                                
                                                </div>
                                                
                                                </div>

                                                
                                                
                                            </div>
                                        </form>
                                            
                                        </div>
                                    </div>
                                </div><!-- .card-preview -->
                                
                            </div><!-- .nk-block -->
                           
                            
                            
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section('js')

        <script>
            $(document).ready(function () {
    
    
            })
        </script>
        @endsection