
  
        @extends('layouts.app')
            <!-- wrap @s -->
            @section('content')
           
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Overview</h3>
                                            <div class="nk-block-des text-soft">
                                                <p>Welcome to Dairi-Delights Admin Portal.</p>
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <li>
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-white btn-dim btn-outline-light" data-toggle="dropdown"><em class="d-none d-sm-inline icon ni ni-calender-date"></em><span><span class="d-none d-md-inline">Last</span> 30 Days</span><em class="dd-indc icon ni ni-chevron-right"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                        <li><a href="#"><span>Last 30 Days</span></a></li>
                                                                        <li><a href="#"><span>Last 6 Months</span></a></li>
                                                                        <li><a href="#"><span>Last 1 Years</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="nk-block-tools-opt"><a href="#" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Reports</span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    <div class="row g-gs">
                                        <div class="col-xxl-6">
                                            <div class="row g-gs">
                                                <div class="col-lg-6 col-xxl-12">
                                                    <div class="card card-bordered">
                                                        <div class="card-inner">
                                                              
                                                               <?php $report->render(); ?>
                                                            
                                                    </div>
                                                    </div>
                                                </div><!-- .col -->
                                                <div class="col-lg-6 col-xxl-12">
                                                    <div class="card card-bordered">
                                                        <div class="card-inner">
                                                               <?php $topTenSkus->render(); ?>
                                                            
                                                    </div>
                                                    </div>
                                                </div><!-- .col -->
                                            </div><!-- .row -->
                                        </div><!-- .col -->
                                        <div class="col-xxl-6">
                                            <div class="card card-bordered h-100">
                                                <div class="card-inner">
                                                    <?php $dailySales->render(); ?>
                                                </div>
                                            </div><!-- .card -->
                                        </div><!-- .col -->
                                        <div class="col-xxl-8">
                                            <div class="card card-bordered card-full">
                                                <div class="card-inner">
                                                    <div class="card-title-group">
                                                        <div class="card-title">
                                                            <h6 class="title"><span class="mr-2">Transaction (Recent 5)</span> <a href="{{route('showTransactions')}}" class="link d-none d-sm-inline">See History</a></h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            <ul class="card-tools-nav">
                                                                {{-- <li><a href="#"><span>Paid</span></a></li>
                                                                <li><a href="#"><span>Pending</span></a></li> --}}
                                                                <li class="active"><a href="{{route('showTransactions')}}"><span>All</span></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-inner p-0 border-top">
                                                    <div class="nk-tb-list nk-tb-orders">
                                                        <div class="nk-tb-item nk-tb-head">
                                                            <div class="nk-tb-col"><span>Order No.</span></div>
                                                            <div class="nk-tb-col tb-col-sm"><span>Customer</span></div>
                                                            <div class="nk-tb-col tb-col-md"><span>Date</span></div>
                                                            <div class="nk-tb-col"><span>Amount</span></div>
                                                            <div class="nk-tb-col"><span class="d-none d-sm-inline">Status</span></div>
                                                            <div class="nk-tb-col"><span>&nbsp;</span></div>
                                                        </div>
                                                        @foreach ($Transactions as $tran)
                                                            
                                                        
                                                        <div class="nk-tb-item">
                                                            <div class="nk-tb-col">
                                                                <span class="tb-lead"><a href="#">#{{$tran->TransactionID}}</a></span>
                                                            </div>
                                                            <div class="nk-tb-col tb-col-sm">
                                                                <div class="user-card">
                                                                   
                                                                    <div class="user-name">
                                                                        <span class="tb-lead">{{$tran->tran_customer_details->first_name}} {{$tran->tran_customer_details->lastname}}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="nk-tb-col tb-col-md">
                                                                <span class="tb-sub">{{date("d M y",strtotime($tran->DateCreated))}}</span>
                                                            </div>
                                                          
                                                            <div class="nk-tb-col">
                                                                <span class="tb-sub tb-amount"> {{number_format($tran->AmountPaid,2)}} <span>{{$tran->currency_code}}</span></span>
                                                            </div>
                                                            <div class="nk-tb-col">
                                                                <span class="badge badge-dot badge-dot-xs badge-info">{{$tran->order_status}}</span>
                                                            </div>
                                                            <div class="nk-tb-col nk-tb-col-action">
                                                                <div class="dropdown">
                                                                    <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-xs">
                                                                        <ul class="link-list-plain">
                                                                            <li><a href="{{route('TransactionDetails',$tran->TransactionID)}}">View Details</a></li>
                                                                           
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        
                                                    </div>
                                                </div>

                                            </div><!-- .card -->
                                        </div><!-- .col -->
                    
                                       
                                        <div class="col-lg-6 col-xxl-4">
                                            <div class="card card-bordered h-100">
                                                <div class="card-inner border-bottom">
                                                    <div class="card-title-group">
                                                        <div class="card-title">
                                                            <h6 class="title">Exchange Rate Logs</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            {{-- <a href="html/subscription/tickets.html" class="link">View All</a> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-inner">
                                                    <div class="timeline">
                                                        {{-- <h6 class="timeline-head">November, 2019</h6> --}}
                                                        <ul class="timeline-list">
                                                            @foreach ($exchangeRate as $item)
                                                            <li class="timeline-item">
                                                                <div class="timeline-status bg-primary is-outline"></div>
                                                                <div class="timeline-date">{{date("M,y", strtotime($item->created_at))}} <em class="icon ni ni-alarm-alt"></em></div>
                                                                <div class="timeline-data">
                                                                    <h6 class="timeline-title">Exchange rate Log</h6>
                                                                    <div class="timeline-des">
                                                                        <p>{{$item->comment}}</p>
                                                                        <span class="time">{{date("d, D M y @ H:i A", strtotime($item->created_at))}}</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            @endforeach
                                                          
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div><!-- .col -->

                                        <div class="col-lg-6 col-xxl-4">
                                            <div class="card card-bordered h-100">
                                                <div class="card-inner border-bottom">
                                                    <div class="card-title-group">
                                                        <div class="card-title">
                                                            <h6 class="title">Product Movement Logs</h6>
                                                        </div>
                                                        <div class="card-tools">
                                                            {{-- <a href="html/subscription/tickets.html" class="link">View All</a> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-inner">
                                                    <div class="timeline">
                                                        {{-- <h6 class="timeline-head">November, 2019</h6> --}}
                                                        <ul class="timeline-list">
                                                            @foreach ($productMovement as $item)
                                                            <li class="timeline-item">
                                                                <div class="timeline-status bg-primary is-outline"></div>
                                                                <div class="timeline-date">{{date("M,y", strtotime($item->created_at))}} <em class="icon ni ni-alarm-alt"></em></div>
                                                                <div class="timeline-data">
                                                                    <h6 class="timeline-title">{{$item->MovementType}}, {{$item->ProductAuditProuduct->name}}</h6>
                                                                    <div class="timeline-des">
                                                                        <p>{{$item->Description}} of {{$item->Qty}} {{$item->Qty == 1 ? 'unit' : 'units' }} from {{$item->old_qty}} to {{$item->new_qty}}</p>
                                                                        <span class="time">{{date("d, D M y @ H:i A", strtotime($item->created_at))}}</span>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            @endforeach
                                                          
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div><!-- .col -->
                                    </div><!-- .row -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
              
          
        
  
@endsection

@section('js')
<script src="{{asset('theme')}}/js/charts/gd-general.js?ver=1.4.0"></script>
@endsection