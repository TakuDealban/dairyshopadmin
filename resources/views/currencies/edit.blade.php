 
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h4 class="title nk-block-title">Update Currency Ex Rate</h4> 
                                        <div class="nk-block-des">
                                            <p class="alert alert-fill alert-info alert-icon"> <strong>BASE CURRENCY:  {{ config('app.BASE_CURRENCY') }} </strong></p>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <div class="preview-block">
                                            {{-- <span class="preview-title-lg overline-title">Default Preview</span> --}}
                                        <form action="{{route('updateexchangerate',$Currency->id)}}" method="POST" enctype="multipart/form-data">
                                          @csrf
                                            <div class="row gy-4">
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-01">Exchange Currency Code</label>
                                                        <div class="form-control-wrap">
                                                        <input type="text" name="forexCurrency" class="form-control form-control-lg @error('forexCurrency') is-invalid @enderror" value="{{$Currency->forexCurrency}}" id="currency_code" placeholder="Enter  Currency Code" required>
                                                            @error('forexCurrency')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Currency Description Code</label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">United States Dollars</span>
                                                            </div>
                                                        <input type="text" name="forex_description" class="form-control  form-control-lg @error('forex_description') is-invalid @enderror" value="{{$Currency->forex_description}}" id="default-05" placeholder="Enter Description To described your currency">
                                                            @error('forex_description')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                              

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Exchange Rate</label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">1 {{config('app.BASE_CURRENCY')}} : {{$Currency->exchangeRate}} {{$Currency->forexCurrency}}</span>
                                                            </div>
                                                        <input type="number" name="exchangeRate" step="0.0001" class="form-control  form-control-lg @error('exchangeRate') is-invalid @enderror" value="{{$Currency->exchangeRate}}" id="exchangeRate" placeholder="Enter  product exchangeRate" required>
                                                            @error('exchangeRate')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">                                                     
                                                        <div class="form-control-wrap helperText text-success">                                                            
                                                        </div>
                                                    </div>
                                                </div>

                                                <input type="hidden" name="baseCurrency" value="{{ config('app.BASE_CURRENCY') }}">

                                               
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-lg btn-primary">Update Currency Exchange</button>
                                                    </div>
                                                
                                                </div>
                                                
                                                </div>

                                                
                                                
                                            </div>
                                        </form>
                                            
                                        </div>
                                    </div>
                                </div><!-- .card-preview -->
                                
                            </div><!-- .nk-block -->
                           
                            
                            
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section('js')

        <script>
            $(document).ready(function () {
    
    $(".helperText").hide();
    
                $('#exchangeRate').on('keyup',function(){
                    var exCurr = $("#currency_code").val();
                    if(exCurr === "NULL" || exCurr === ""){
                        alert("Exchange Rate Currency can not be empty");
                        $(".helperText").hide("slow");
                    }else{
                        var exRate = $(this).val();
            var baseCur = "{{ config('app.BASE_CURRENCY') }}";
            // alert(baseCur);
            
            var statement = "NB: 1 "+baseCur+" = " +  exRate + " " + exCurr;
            $(".helperText").text(statement);
            $(".helperText").show("slow");
                    }
            
        });
    
             
            })
        </script>
        @endsection