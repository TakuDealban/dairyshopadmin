
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                        <div class="col-md-9">
                                            <h4 class="nk-block-title">SAP Configs </h4>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                       
                                        <div class="nk-block-des col-md-3">
                                            {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
                                        {{-- <a class="btn btn-primary pull-right" href="{{route('newcategory')}}">Companu</a> --}}
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#tabItem5"><em class="icon ni ni-setting-alt-fill"></em><span>Web Services Configs</span></a>
                                            </li>
                                           
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#tabItem6"><em class="icon ni  ni-setting-alt-fill"></em><span>Pick Up Points</span></a>
                                            </li>
                                           
                                            
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tabItem5">
                                                <form action="{{route('updatewebservice')}}" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                      <div class="row gy-4">
                                                          <div class="col-sm-12">
                                                              <div class="form-group">
                                                                  <label class="form-label" for="default-01">Webservice URL</label>
                                                                  <div class="form-control-wrap">
                                                                  <input type="text" name="url" class="form-control form-control-lg @error('url') is-invalid @enderror" value="{{$webservices != null ? $webservices->url : ''}}" id="default-01" placeholder="Enter Webservice url" required>
                                                                      @error('url')
                                                                      <span class="invalid-feedback" role="alert">
                                                                          <strong>{{ $message }}</strong>
                                                                      </span>
                                                                  @enderror
                                                                  </div>
                                                              </div>
                                                          </div>

                                                          <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Proxy IP Address</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        <span class="overline-title">Proxy IP Address</span>
                                                                    </div>
                                                                <input type="text" name="proxy_ip" class="form-control  form-control-lg @error('proxy_ip') is-invalid @enderror" value="{{$webservices != null ? $webservices->proxy_ip : ''}}" id="default-05" placeholder="Enter webservices IP Address" required>
                                                                    @error('proxy_ip')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                          <div class="form-group">
                                                              <label class="form-label" for="default-05">Proxy Port</label>
                                                              <div class="form-control-wrap">
                                                                  <div class="form-text-hint">
                                                                      <span class="overline-title">Proxy Port</span>
                                                                  </div>
                                                              <input type="text" name="proxy_port" class="form-control  form-control-lg @error('proxy_port') is-invalid @enderror" value="{{$webservices != null ? $webservices->proxy_port : ''}}" id="default-05" placeholder="Enter IP Address PORT " >
                                                                  @error('proxy_port')
                                                                  <span class="invalid-feedback" role="alert">
                                                                      <strong>{{ $message }}</strong>
                                                                  </span>
                                                              @enderror
                                                              </div>
                                                          </div>
                                                      </div>
                                                         

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Proxy Login</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        
                                                                    </div>
                                                                <input type="text" name="proxy_login" class="form-control  form-control-lg @error('proxy_login') is-invalid @enderror" value="{{$webservices != null ? $webservices->proxy_login : ''}}" id="default-05" placeholder="Enter Proxy Login (Browser)" required>
                                                                    @error('proxy_login')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">Proxy Password</label>
                                                                <div class="form-control-wrap">
                                                                 
                                                                <input type="text" name="proxy_password" class="form-control  form-control-lg @error('proxy_password') is-invalid @enderror" value="{{$webservices != null ? $webservices->proxy_password : ''}}" id="default-05" placeholder="Enter proxy password" required>
                                                                    @error('proxy_password')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">User Login</label>
                                                                <div class="form-control-wrap">
                                                                  
                                                                <input type="text" name="login" class="form-control  form-control-lg @error('login') is-invalid @enderror" value="{{$webservices != null ? $webservices->login : ''}}" id="default-05" placeholder="Enter User login -  SAP Username" required>
                                                                    @error('login')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="form-label" for="default-05">User Password</label>
                                                                <div class="form-control-wrap">
                                                                    <div class="form-text-hint">
                                                                        <span class="overline-title">Password</span>
                                                                        </div>
                                                                <input type="text" name="password" class="form-control  form-control-lg @error('password') is-invalid @enderror" value="{{$webservices != null ? $webservices->password : ''}}" id="default-05" placeholder="Enter User password - SAP Password" required>
                                                                    @error('password')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        
                                                          
                                                          
                                                          
          
                                                          <div class="col-sm-8">
                                                              <div class="form-group">
                                                                  <button type="submit" class="btn btn-lg btn-primary">Update Webservice Data</button>
                                                              </div>
                                                          
                                                          </div>
                                                          
                                                      </div>
                                                  </form>
                                            </div>
                                            
                                            <div class="tab-pane" id="tabItem6">
                                            <a href="{{route('newpickup')}}" class="btn btn-primary right">New Storage Location</a> <br><br>
                                                <table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                            
                                                            <th class="nk-tb-col"><span class="sub-text">Storage Reference</span></th>
                                                            <th class="nk-tb-col tb-col-mb"><span class="sub-text">Plant</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Storage Location</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Order Type</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Pick Up Point</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Created Date</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text"></span></th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($pickuppoints as $point)
                                                        <tr class="nk-tb-item">
                                                           
                                                            <td class="nk-tb-col">
                                                            <span>STLOC_{{$point->id}}</span>
                                                            </td>
                                                            <td class="nk-tb-col tb-col-mb">
                                                                <div class="user-info">
                                                                <span class="tb-lead">{{$point->plant}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                                   
                                                                </div>
                                                            </td>
                                                            <td class="nk-tb-col tb-col-md">
                                                                <span>{{$point->storage_location}}</span>
                                                            </td>
                                                            <td class="nk-tb-col tb-col-lg">
                                                                <ul class="list-status">
                                                                <li><em class="icon text-success ni ni-check-circle"></em> <span>{{$point->order_type}}</span></li>
                                                                    
                                                                </ul>
                                                            </td>
                                                            <td class="nk-tb-col tb-col-md">
                                                                <span>{{$point->pick_up_point}}</span>
                                                            </td>
                                                            <td class="nk-tb-col tb-col-lg">
                                                                <span>{{$point->created_at->format('d M y')}}</span>
                                                            </td>
                                                           
                                                            <td class="nk-tb-col nk-tb-col-tools">
                                                                <ul class="nk-tb-actions gx-1">
                                                                    {{-- <li class="nk-tb-action-hidden">
                                                                        <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Wallet">
                                                                            <em class="icon ni ni-wallet-fill"></em>
                                                                        </a>
                                                                    </li>
                                                                    <li class="nk-tb-action-hidden">
                                                                        <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Send Email">
                                                                            <em class="icon ni ni-mail-fill"></em>
                                                                        </a>
                                                                    </li>
                                                                    <li class="nk-tb-action-hidden">
                                                                        <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Suspend">
                                                                            <em class="icon ni ni-user-cross-fill"></em>
                                                                        </a>
                                                                    </li> --}}
                                                                    <li>
                                                                        <div class="drodown">
                                                                            <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                                <ul class="link-list-opt no-bdr">
                                                                                <li><a href="{{route('editpoint',$point->id)}}"><em class="icon ni ni-focus"></em><span>Update Storage Location</span></a></li>
                                                                                    <li><a href="#"><em class="icon ni ni-focus"></em><span>Quick View</span></a></li>
                                                                                    <li><a href="#"><em class="icon ni ni-eye"></em><span>View Logs</span></a></li>
                                                                                    <li><a href="#"><em class="icon ni ni-eye"></em><span>View Products</span></a></li>
                                                                                 
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr><!-- .nk-tb-item  -->
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                           
                                           
                                        </div>
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection