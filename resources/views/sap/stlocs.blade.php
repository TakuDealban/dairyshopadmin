
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                        <div class="col-md-9">
                                            <h4 class="nk-block-title">Storage Locations </h4>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                       
                                        <div class="nk-block-des col-md-3">
                                            {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
                                        <a class="btn btn-primary pull-right" href="{{route('newpickup')}}">Set Up Storage Location</a>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        {{-- <a href="{{route('newpickup')}}" class="btn btn-primary right">New Storage Location</a> <br><br> --}}
                                        <table class="datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                            <thead>
                                                <tr class="nk-tb-item nk-tb-head">
                                                    
                                                   
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">ST Loc / Plant</span></th>
                                                    {{-- <th class="nk-tb-col tb-col-md"><span class="sub-text">Storage Location</span></th> --}}
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Order Type</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Pick Up Point</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Pick Up City</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text"># of Products</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">SAP Account #</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Created Date</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text"></span></th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($pickuppoints as $point)
                                                <tr class="nk-tb-item">
                                                   
                                           
                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$point->storage_location}}/{{$point->plant}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                           
                                                        </div>
                                                    </td>
                                                   
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <ul class="list-status">
                                                        <li><em class="icon text-success ni ni-check-circle"></em> <span>{{$point->order_type}}</span></li>
                                                            
                                                        </ul>
                                                    </td>
                                                    <td class="nk-tb-col tb-col-md">
                                                        <span>{{$point->pick_up_point}}</span>
                                                    </td>
                                                    <td class="nk-tb-col tb-col-md">
                                                        <span>{{$point->city}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-md">
                                                        <span>{{$point->storage_products->count()}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-md">
                                                        <span>USD:{{$point->usd_customer_account}}/ZWL:{{$point->zwl_customer_account}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{$point->created_at->format('d M y')}}</span>
                                                    </td>
                                                   
                                                    <td class="nk-tb-col nk-tb-col-tools">
                                                        <ul class="nk-tb-actions gx-1">
                                                            {{-- <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Wallet">
                                                                    <em class="icon ni ni-wallet-fill"></em>
                                                                </a>
                                                            </li>
                                                            <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Send Email">
                                                                    <em class="icon ni ni-mail-fill"></em>
                                                                </a>
                                                            </li>
                                                            <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Suspend">
                                                                    <em class="icon ni ni-user-cross-fill"></em>
                                                                </a>
                                                            </li> --}}
                                                            <li>
                                                                <div class="drodown">
                                                                    <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        <ul class="link-list-opt no-bdr">
                                                                        <li><a href="{{route('editpoint',$point->id)}}"><em class="icon ni ni-focus"></em><span>Update Storage Location</span></a></li>
                                                                     
                                                                        <li><a href="{{route('vwStorageLocProducts',$point->storage_location)}}"><em class="icon ni ni-eye"></em><span>View Products In {{$point->storage_location}}</span></a></li>
                                                                         
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr><!-- .nk-tb-item  -->
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection