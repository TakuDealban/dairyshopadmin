
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="nk-block-title">Storage Location Products </h4>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                            @elseif(session('error'))
                                            <br>
                                            <p class="alert alert-fill alert-danger alert-icon">{{ session('error') }}</p>
                                         
                                            @endif
                                        </div>
                                       
                                        <div class="nk-block-des col-md-3">
                                            {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
                                        {{-- <a class="btn btn-default pull-right" href="{{route('newproduct')}}">New Prod</a> --}}
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <table class="table-responsive datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                            <thead>
                                                <tr class="nk-tb-item nk-tb-head">
                                                 
                                                    <th class="nk-tb-col"><span class="sub-text">Product Image</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Product Name</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Product VAT Code</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Qty</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Currency</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Price</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Upload Date</span></th>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($products as $prod)
                                                <tr class="nk-tb-item">
                                                    
                                                    <td class="nk-tb-col">
                                                        <div class="user-card">
                                                            <div class="user-avatar bg-dim-primary d-none d-sm-flex">
                                                                <span><img src="/uploads/products/{{$prod->PriceListProduct->imagePath}}" alt=""></span>
                                                            </div>
                                                         
                                                        </div>
                                                    </td>
                                                   
                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$prod->PriceListProduct->name}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                        <span>{{$prod->PriceListProduct->product_code}}</span>
                                                        </div>
                                                    </td>
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <ul class="list-status">
                                                        <li><em class="icon text-success ni ni-check-circle"></em> <span> {{$prod->PriceListProduct->VAT_CODE}}</span></li>
                                                            
                                                        </ul>
                                                        
                                                    </td>

                                                  

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <ul class="list-status">
                                                        <li><em class="icon text-success ni ni-check-circle"></em> <span> {{$prod->qty}}</span></li>
                                                            
                                                        </ul>
                                                        
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <ul class="list-status">
                                                        <li><em class="icon text-success ni ni-check-circle"></em> <span> {{$prod->currency}}</span></li>
                                                            
                                                        </ul>
                                                        
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <ul class="list-status">
                                                        <li><em class="icon text-success ni ni-check-circle"></em> <span> {{$prod->price}}</span></li>
                                                            
                                                        </ul>
                                                        
                                                    </td>
                                                    
                                                  
                                                    
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{$prod->created_at->format('d M y')}}</span>
                                                    </td>
                                                   
                                                    
                                                </tr><!-- .nk-tb-item  -->
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection