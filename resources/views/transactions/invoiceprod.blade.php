
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                        <div class="col-md-9">
                                            <h4 class="nk-block-title">Invoice Products </h4>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                       
                                        <div class="nk-block-des col-md-3">
                                            {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
                                        {{-- <a class="btn btn-primary pull-right" href="{{route('sync_invoices')}}">Upload Invoices</a> --}}
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <table class="table-responsive datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                            <thead>
                                                <tr class="nk-tb-item nk-tb-head">
                                                    {{-- <th class="nk-tb-col nk-tb-col-check">
                                                        <div class="custom-control custom-control-sm custom-checkbox notext">
                                                            <input type="checkbox" class="custom-control-input" id="uid">
                                                            <label class="custom-control-label" for="uid"></label>
                                                        </div>
                                                    </th> --}}
                                                    <th class="nk-tb-col"><span class="sub-text">Product Code</span></th>
                                                    <th class="nk-tb-col tb-col-md"><span class="sub-text">Product Name</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Selling Price</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Qty Sold</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Line Total </span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Line VAT</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Net Total</span></th>
                                                    {{-- <th class="nk-tb-col nk-tb-col-tools text-right">                                                         --}}
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($trans_products as $tran)
                                                <tr class="nk-tb-item">
                                  
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{$tran->rec_item_product->product_code}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{$tran->rec_item_product->name}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span> {{number_format($tran->selling_price,2)}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span> {{$tran->quantity}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span> {{number_format($tran->linetotal,2)}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span> {{number_format($tran->line_vat,2)}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span> {{number_format($tran->after_vat_total,2)}}</span>
                                                    </td>

                                                    {{-- <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$tran->Phone}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                        <span>{{$tran->Email}}</span>
                                                        </div>
                                                    </td> --}}

                                                    
                                                </tr><!-- .nk-tb-item  -->
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection