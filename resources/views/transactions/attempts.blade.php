
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                        <div class="col-md-9">
                                            <h4 class="nk-block-title">Attempts </h4>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                       
                                        <div class="nk-block-des col-md-3">
                                            {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
                                        {{-- <a class="btn btn-default pull-right" href="{{route('newproduct')}}">New Prod</a> --}}
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <table class="table-responsive datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                            <thead>
                                                <tr class="nk-tb-item nk-tb-head">
                                                    {{-- <th class="nk-tb-col nk-tb-col-check">
                                                        <div class="custom-control custom-control-sm custom-checkbox notext">
                                                            <input type="checkbox" class="custom-control-input" id="uid">
                                                            <label class="custom-control-label" for="uid"></label>
                                                        </div>
                                                    </th> --}}
                                                    <th class="nk-tb-col"><span class="sub-text">Request Number</span></th>
                                                    <th class="nk-tb-col tb-col-md"><span class="sub-text">Integration Reference</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Request Total</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Customer</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Request Summary</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Created Date</span></th>
                                                    <th class="nk-tb-col nk-tb-col-tools text-left">                                                        
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($Transactions as $tran)
                                                <tr class="nk-tb-item">
                                  
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{$tran->request_reference}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{$tran->paynow_reference}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{config('app.BaseCurrency')}} {{number_format($tran->after_charges_bill,2)}}</span>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$tran->phone}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                        <span>{{$tran->email}}</span>
                                                        </div>
                                                    </td>
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <ul class="list-status">
                                                        <li><em class="icon text-success ni ni-check-circle"></em> <span><a href="{{($tran->poll_url)}}" target="_blank"> Open Poll Reference</a></span></li>
                                                            
                                                        </ul>
                                                        
                                                    </td>
                                         
                                                    
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{date("d M y",strtotime($tran->created_at))}}</span>
                                                    </td>
                                                   
                                                    <td class="nk-tb-col nk-tb-col-tools">
                                                        <ul class="nk-tb-actions gx-1">
                                                            {{-- <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Wallet">
                                                                    <em class="icon ni ni-wallet-fill"></em>
                                                                </a>
                                                            </li>
                                                            <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Send Email">
                                                                    <em class="icon ni ni-mail-fill"></em>
                                                                </a>
                                                            </li>
                                                            <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Suspend">
                                                                    <em class="icon ni ni-user-cross-fill"></em>
                                                                </a>
                                                            </li> --}}
                                                            <li>
                                                                <div class="drodown">
                                                                    <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        <ul class="link-list-opt no-bdr">
                                                                        <li><a href=""><em class="icon ni ni-focus"></em><span>View Summary</span></a></li>
                                                                            <li><a href="#"><em class="icon ni ni-focus"></em><span>Quick View</span></a></li>
                                                                            <li><a href="#"><em class="icon ni ni-eye"></em><span>View Logs</span></a></li>
                                                                            <li><a href="#"><em class="icon ni ni-eye"></em><span>View Products</span></a></li>
                                                                         
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr><!-- .nk-tb-item  -->
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection