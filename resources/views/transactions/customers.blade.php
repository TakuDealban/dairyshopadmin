
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                        <div class="col-md-9">
                                            <h4 class="nk-block-title">Customers </h4>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                       
                                        <div class="nk-block-des col-md-3">
                                            {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
                                        {{-- <a class="btn btn-default pull-right" href="{{route('newproduct')}}">New Prod</a> --}}
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <table class="table-responsive datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                            <thead>
                                                <tr class="nk-tb-item nk-tb-head">
                                                    {{-- <th class="nk-tb-col nk-tb-col-check">
                                                        <div class="custom-control custom-control-sm custom-checkbox notext">
                                                            <input type="checkbox" class="custom-control-input" id="uid">
                                                            <label class="custom-control-label" for="uid"></label>
                                                        </div>
                                                    </th> --}}
                                                    <th class="nk-tb-col"><span class="sub-text">Name</span></th>
                                                    <th class="nk-tb-col tb-col-md"><span class="sub-text">Country</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Phone</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Town</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Transactions</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Created Date</span></th>                                                        
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($Transactions as $tran)
                                                <tr class="nk-tb-item">
                                  
                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$tran->first_name}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                        <span>{{$tran->last_name}}</span>
                                                        </div>
                                                    </td>
                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$tran->country}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                        <span>{{$tran->company}}</span>
                                                        </div>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$tran->phone}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                        <span>{{$tran->email}}</span>
                                                        </div>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$tran->town}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                        <span>{{$tran->zip}}</span>
                                                        </div>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{count($tran->CustomerTransactions)}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                        <span>{{config('app.BASE_CURRENCY')}} {{$tran->CustomerTransactions->sum('AmountPaid')}}</span>
                                                        </div>
                                                    </td>                         
                                         
                                                    
                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{date("d M y",strtotime($tran->created_at))}}</span>
                                                    </td>
                                                   
                                                    
                                                </tr><!-- .nk-tb-item  -->
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection