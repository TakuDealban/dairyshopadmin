 
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                    <h4 class="title nk-block-title">Change Invoice Status : {{$invoice_number}}</h4> 
                                        <div class="nk-block-des">
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <div class="preview-block">
                                            {{-- <span class="preview-title-lg overline-title">Default Preview</span> --}}
                                        <form action="{{route('processInvoiceStatus')}}" method="POST" enctype="multipart/form-data">
                                          @csrf
                                            <div class="row gy-4">
                                                
                                                

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label">Update Status </label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select form-control-lg @error('order_status') is-invalid @enderror" name="order_status" data-ui="lg" data-search="on" required>
                                                            <option value="Processed">Processed</option>
                                                            <option value="Awaiting Delivery">Awating Delivery</option>
                                                            <option value="Delivered">Delivered</option>
                                                            <option value="Dispatched">Dispatched</option>
                                                            <option value="Cancelled">Cancelled</option>
                                                            <option value="Returns">Returned (Void)</option>
                                                            
                                                            </select>

                                                            @error('order_status')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-01">Customer Update Channel</label>
                                                        <div class="form-control-wrap">
                                                            <label class="radio-inline"><input type="radio" name="optradio" value="dont">&nbsp;&nbsp; Don't Share Status Change</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline"><input type="radio" name="optradio" value="sms">&nbsp;&nbsp; SMS</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline"><input type="radio" name="optradio" value="email"> &nbsp;&nbsp; Email</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="radio-inline"><input type="radio" name="optradio" value="both">&nbsp;&nbsp; SMS AND EMAIL</label>
                                                            @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                            <input name="invoice_number" type="hidden" value="{{$invoice_number}}">

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-01">Comment</label>
                                                        <div class="form-control-wrap">
                                                        <input type="text" name="comment" class="form-control form-control-lg @error('comment') is-invalid @enderror" value="{{old('comment')}}" id="default-01" placeholder="Enter change status comment. E.G. Invoice ready for delivery or any cancellation reason.">
                                                            @error('comment')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                

                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-lg btn-primary">Update Invoice Status</button>
                                                    </div>
                                                
                                                </div>
                                                
                                            </div>
                                        </form>
                                            
                                        </div>
                                    </div>
                                </div><!-- .card-preview -->
                                
                            </div><!-- .nk-block -->
                           
                            
                            
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section('js')
    <script> 
    $(document).ready(function(){
        const format = (num, decimals) => num.toLocaleString('en-US', {
   minimumFractionDigits: 2,      
   maximumFractionDigits: 2,
});

       $(".case_value").on('keyup',function(){
           var unit_price = parseFloat($(".unit_price").val());
           var case_total = 0;
           if(unit_price > 0){
            case_total = unit_price * $(this).val();
           }
          
           $(".case_price").val(case_total.toFixed(2))
       })
    });

    </script>
            
        @endsection