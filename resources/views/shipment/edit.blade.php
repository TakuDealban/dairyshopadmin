 
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h4 class="title nk-block-title">Update Shipping Plan </h4> 
                                        <div class="nk-block-des">
                                            {{-- <p class="alert alert-fill alert-info alert-icon"> <strong> {{ config('app.BASE_CURRENCY') }} Paired Currency combinations </strong> : {{ $currencies }}</p> --}}
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <div class="preview-block">
                                            {{-- <span class="preview-title-lg overline-title">Default Preview</span> --}}
                                        <form action="{{route('updateplan',$plan->id)}}" method="POST" enctype="multipart/form-data">
                                          @csrf
                                            <div class="row gy-4">
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-01">Destination Location</label>
                                                        <div class="form-control-wrap">
                                                        <input type="text" name="location_to" class="form-control form-control-lg @error('location_to') is-invalid @enderror" value="{{$plan->location_to}}" id="currency_code" placeholder="Enter  Destination Location" required>
                                                            @error('location_to')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Shipment Charge Currency</label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select form-control-lg @error('shipment_charge_currency') is-invalid @enderror" name="shipment_charge_currency" data-ui="lg" data-search="on" required>
                                                               @foreach ($currencies as $currency)
                                                            <option value="{{$currency->id}}" {{$plan->shipment_charge_currency == $currency->id ? "selected" : ""}}>{{$currency->forexCurrency}}</option>
                                                               @endforeach
                                                            </select>

                                                            @error('shipment_charge_currency')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Shipment Charge</label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">0.00</span>
                                                            </div>
                                                        <input type="text" name="shipment_charge" class="form-control  form-control-lg @error('shipment_charge') is-invalid @enderror" value="{{$plan->shipment_charge}}" id="default-05" placeholder="Enter shipment charge">
                                                            @error('shipment_charge')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Handling / Other Costs Currency (optional)</label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select form-control-lg @error('handling_fees_currency') is-invalid @enderror" name="handling_fees_currency" data-ui="lg" data-search="on">
                                                                <option value="0">Select Handling currency (optional)</option>
                                                                @foreach ($currencies as $currency)
                                                                <option value="{{$currency->id}}" {{$plan->handling_fees_currency == $currency->id ? "selected" : ""}} >{{$currency->forexCurrency}}</option>
                                                                   @endforeach
                                                            </select>

                                                            @error('handling_fees_currency')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="default-05">Handling / Other Costs</label>
                                                        <div class="form-control-wrap">
                                                            <div class="form-text-hint">
                                                                <span class="overline-title">0.00</span>
                                                            </div>
                                                        <input type="text" name="handling_fees" class="form-control  form-control-lg @error('handling_fees') is-invalid @enderror" value="{{$plan->handling_fees}}" id="default-05" placeholder="Enter handling charge or other related route costs">
                                                            @error('handling_fees')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                              

                                            <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-lg btn-primary">Save Plan</button>
                                                    </div>
                                                
                                                </div>
                                                
                                                </div>

                                                
                                                
                                            </div>
                                        </form>
                                            
                                        </div>
                                    </div>
                                </div><!-- .card-preview -->
                                
                            </div><!-- .nk-block -->
                           
                            
                            
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section('js')

        <script>
            $(document).ready(function () {
    
    
            })
        </script>
        @endsection