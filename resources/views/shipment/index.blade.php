
        @extends('layouts.app')
        <!-- wrap @s -->
        @section('content')

        <div class="nk-content ">
            <div class="container-fluid">
                <div class="nk-content-inner">
                    <div class="nk-content-body">
                        <div class="components-preview wide-md mx-auto">
                           
                            
                            <div class="nk-block nk-block-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <div class="row">
                                        <div class="col-md-9">
                                            <h4 class="nk-block-title">Shipment Plans Rates </h4>
                                            @if (session('message'))
                                            <br>
                                            <p class="alert alert-fill alert-success alert-icon">{{ session('message') }}</p>
                                       @endif
                                        </div>
                                       
                                        <div class="nk-block-des col-md-3">
                                            {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
                                        {{-- <a class="btn btn-default pull-right" href="{{route('newproduct')}}">New Prod</a> --}}
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <table class="table-responsive datatable-init nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                            <thead>
                                                <tr class="nk-tb-item nk-tb-head">
                                                    {{-- <th class="nk-tb-col nk-tb-col-check">
                                                        <div class="custom-control custom-control-sm custom-checkbox notext">
                                                            <input type="checkbox" class="custom-control-input" id="uid">
                                                            <label class="custom-control-label" for="uid"></label>
                                                        </div>
                                                    </th> --}}
                                                    {{-- <th class="nk-tb-col"><span class="sub-text">Shipment Config Reference #</span></th> --}}
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Configured Destination</span></th>
                                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">Expected Shipment Charge </span></th>
                                                    <th class="nk-tb-col tb-col-md"><span class="sub-text">Expected Handling Cost</span></th>
                                                    <th class="nk-tb-col tb-col-lg"><span class="sub-text">Date Last Updated</span></th>
                                                    <th class="nk-tb-col nk-tb-col-tools text-right">
                                                        <div class="dropdown">
                                                            <a href="#" class="btn btn-xs btn-outline-light btn-icon dropdown-toggle" data-toggle="dropdown" data-offset="0,5"><em class="icon ni ni-plus"></em></a>
                                                            <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">
                                                                <ul class="link-tidy sm no-bdr">
                                                                    <li>
                                                                    <a href="{{route('newplan')}}" class="btn btn-default"> <i class="icon ni ni-plus"></i> &nbsp; Add</a>    
                                                                    </li>
    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($plans as $plan)
                                                <tr class="nk-tb-item">
                                                   
                                                    {{-- <td class="nk-tb-col tb-col-lg">
                                                        <span>REF # {{$plan->id}}</span>
                                                    </td> --}}

                                                    <td class="nk-tb-col tb-col-mb">
                                                        <div class="user-info">
                                                        <span class="tb-lead">{{$plan->location_to}}<span class="dot dot-success d-md-none ml-1"></span></span>
                                                      
                                                        </div>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <ul class="list-status">
                                                            @if($plan->id % 2 != 0)
                                                        <li ><em class="icon text-success ni ni-check-circle"></em> <span>{{$plan->ship_charge_currency->forexCurrency}} {{$plan->shipment_charge}}</span></li>
                                                            @else
                                                            <li ><em class="icon text-success ni ni-check-circle"></em> <span>{{$plan->ship_charge_currency->forexCurrency}} {{$plan->shipment_charge}}</span></li>
                                                       
                                                            @endif
                                                        </ul>
                                                    </td>

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <ul class="list-status">
                                                            @if($plan->id % 2 != 0)
                                                        <li><em class="icon text-success ni ni-check-circle"></em> <span>{{$plan->handling_fees_currency != 0 ? $plan->handling_charge_currency->forexCurrency." ".$plan->handling_fees : "No Fees"}}</span></li>
                                                            @else
                                                            <li ><em class="icon text-success ni ni-check-circle"></em> <span>{{$plan->handling_fees_currency != 0 ? $plan->handling_charge_currency->forexCurrency." ".$plan->handling_fees : "No Fees"}} </span></li>
                                                       
                                                            @endif
                                                        </ul>
                                                    </td>
                                                   
                                                    

                                                    <td class="nk-tb-col tb-col-lg">
                                                        <span>{{$plan->updated_at->format('d M y')}}</span>
                                                    </td>
                                                    {{-- <td class="nk-tb-col tb-col-lg">
                                                        <span>{{$prod->created_at->format('d M y')}}</span>
                                                    </td> --}}
                                                   
                                                    <td class="nk-tb-col nk-tb-col-tools">
                                                        <ul class="nk-tb-actions gx-1">
                                                            {{-- <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Wallet">
                                                                    <em class="icon ni ni-wallet-fill"></em>
                                                                </a>
                                                            </li>
                                                            <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Send Email">
                                                                    <em class="icon ni ni-mail-fill"></em>
                                                                </a>
                                                            </li>
                                                            <li class="nk-tb-action-hidden">
                                                                <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Suspend">
                                                                    <em class="icon ni ni-user-cross-fill"></em>
                                                                </a>
                                                            </li> --}}
                                                            <li>
                                                                <div class="drodown">
                                                                    <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        <ul class="link-list-opt no-bdr">
                                                                        <li><a href="{{route('editplan',$plan->id)}}"><em class="icon ni ni-eye"></em><span>Update Plan</span></a></li>
                                                                            
                                                                        
                                                                         
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr><!-- .nk-tb-item  -->
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- .card-preview -->
                            </div> <!-- nk-block -->
                        </div><!-- .components-preview -->
                    </div>
                </div>
            </div>
        </div>

        @endsection