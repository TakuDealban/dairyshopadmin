<?php

namespace App\Exports;

use App\MainPriceListFromSap;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class BulkExport implements FromQuery,WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return MainPriceListFromSap::all();
    // }

    public function headings(): array
    {
        return [
            'condition_number',
            'condition_type',
            'price_list_type',
            'sales_org',
            'dist_channel',
            'plant',
            'st_loc',
            'valid_from',
            'valid_to',
            'currency',
            'sap_prod_code',
            'product_id',
            'price',
            'price_usd',
            'qty',
            'created_at',
            'created_by',
            'has_cond_number_in_list_I'
        ];
    }

    public function query()
    {
        return MainPriceListFromSap::query()->whereRaw('product_id > 0');

    }
    public function map($bulk): array
    {
        
        return [           
            $bulk->condition_number,
            $bulk->condition_type,
            $bulk->price_list_type,
            $bulk->sales_org,
            $bulk->dist_channel,
            $bulk->plant,
            $bulk->st_loc,
            $bulk->valid_from,
            $bulk->valid_to,
            $bulk->currency,
            $bulk->sap_prod_code,
            $bulk->product_id,
            $bulk->price,
            $bulk->price_usd,
            $bulk->qty,
            $bulk->created_at,
            $bulk->created_by,
            $bulk->has_cond_number_in_list_I
        ];
    }
}
