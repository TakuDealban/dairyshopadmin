<?php

namespace App\Exports;

use App\RecHeader;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class InvoicesExport implements FromQuery,WithHeadings,WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        $date = date("Y-m-d");
        return RecHeader::query()->where("sync_date", ">=" , date('d-m-Y'));
    }

    public function headings(): array
    {
        return [
            'InvoiceNumber',
            'OrderID',
            'SAPBillingNumber',
            'SAPDeliveryNumber',
            'OrderTotal',
            'StorageLocation',
            'SyncDate'
        ];
    }

    public function map($bulk): array
    {
        
        return [           
            $bulk->TransactionID,
            $bulk->OrderID,
            $bulk->billing_no,
            $bulk->delivery_no,
            $bulk->AmountPaid,
            $bulk->storage_location,
            $bulk->sync_date
        ];
    }
}
