<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecHeader extends Model
{
    protected $table = 'recheader';
    public $timestamps = false;
    protected $fillable = ['billing_no','delivery_no','sync_date','TransactionID'];
    
    public function tran_channel(){
        return $this->hasOne(PaymentRequest::class, 'request_reference', 'OrderID'); 
    }

    public function tran_customer_details(){
        return $this->hasOne(BillingDetail::class, 'email', 'Email'); 
    }


    public function trans_products(){
        return $this->hasMany(RecItems::class, 'order_number', 'OrderID');
    }
}
