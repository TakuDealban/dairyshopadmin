<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOfficePriceList extends Model
{
    //
    protected $fillable = ['condition_number','condition_type','price_list_type','sales_office','dist_channel','valid_from','valid_to','currency',
    'sap_prod_code','price','created_by', 'has_cond_number_in_list_I'];
}
