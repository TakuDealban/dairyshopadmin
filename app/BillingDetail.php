<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingDetail extends Model
{
    public function CustomerTransactions()
    {
       return $this->hasMany(RecHeader::class,'Email', 'email');
    }
}
