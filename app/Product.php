<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
                        'name','product_code','price','imagePath','stock_on_hand','category_id','created_by',
                        'VAT_CODE', 'case_price','case_value','sap_product_code', 'category_description', 'UOM'
                        ];

    public function product_category(){
        return $this->hasOne(Category::class,'id','category_id');
    }
}
