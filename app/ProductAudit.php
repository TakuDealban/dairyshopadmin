<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAudit extends Model
{
    protected $table = 'prod_audit';

    public function ProductAuditProuduct()
    {
       return $this->belongsTo(Product::class, 'prod_id', 'id');
    }
}
