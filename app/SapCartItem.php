<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SapCartItem extends Model
{
    //
    protected $fillable = ['plant','storage_loc','sap_product_code','sap_material_desc','quantity','in_master','created_by'];
}
