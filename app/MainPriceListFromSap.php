<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainPriceListFromSap extends Model
{
    //
    protected $fillable = ['condition_number','condition_type','price_list_type','sales_org','dist_channel','valid_from','valid_to','currency',
                            'sap_prod_code','price','price_usd','created_by', 'has_cond_number_in_list_I', 'qty', 'plant', 'st_loc', 'product_id'];


    public function PriceListProduct()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }

}
