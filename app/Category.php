<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','description','image','created_by', 'sap_category_code'];

    public function cat_products(){
        return $this->hasMany(Product::class,'category_id','id');
    }
}
