<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SapWebservicesconfig extends Model
{
    protected $table = 'sap_webservices_configs';
}
