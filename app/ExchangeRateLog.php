<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeRateLog extends Model
{
    protected $table = 'exchange_rates_logs';
}
