<?php

namespace App\classes;

use App\ProductAudit;

class ProductLogs {


    public static function log_product_movement($movement_type, $Description, $product_id, $qty, $old_qty, $new_wqty, $reason){

        $new_log = new ProductAudit();
        $new_log->MovementType = $movement_type;
        $new_log->Description = $Description;
        $new_log->prod_id = $product_id;
        $new_log->Qty = $qty;
        $new_log->old_qty = $old_qty;
        $new_log->new_qty = $new_wqty;
        $new_log->Reason = $reason;
        $new_log->created_by = auth()->user()->id;
        if($new_log->save()){
            return true;
        }
        else{
            return false;
        }

    }
}
