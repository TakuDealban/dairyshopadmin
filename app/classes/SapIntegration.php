<?php

namespace App\classes;

use SoapClient;

class SapIntegration {
       
  protected $client;
  public function __construct($soapWSDL, $proxy_ip, $proxy_port,$proxy_login, $proxy_password, $login, $password )
  {
  
      // $this->client = new SoapClient($soapWSDL,
      //                           array(
      //                             'proxy_host'=>$proxy_ip,
      //                           'proxy_port'=>$proxy_port,
      //                           'proxy_login'=>$proxy_login,
      //                           'proxy_password'=>$proxy_password,
      //                           'login'=>$login,
      //                           'password'=>$password,
      //                            'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | 9,
      //                            ),
      //                       );

      $opts = array(
        'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false)
    );
    // SOAP 1.2 client
    $params = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false, 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180, 'stream_context' => stream_context_create($opts) );


        $this->client = new SoapClient("http://197.155.238.254:8000/sap/bc/srt/wsdl/bndg_5FB2CCCD42D62620E1000000C0A80414/wsdl11/allinone/standard/document?sap-client=700?wsdl",
                            array(
                              'proxy_host'=>"197.155.238.254",
                              'proxy_port'=>"8000",
                              'proxy_login'=>"consult",
                              'proxy_password'=>"Pass1234", 
                              'login'=>"cartuser",
                              'password'=>"Kadoma123",
                              'exceptions' => true,
                              'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | 9,
                              array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false, 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180, 'stream_context' => stream_context_create($opts) ),
                                  'cache_wsdl' => WSDL_CACHE_NONE
                              )                            
                          );
                           
                                                         
  }

    public  function Material_FOR_CART($year){
        $params = array('YEAR'=>$year, 'ZSTOCK' => array());
        $response = $this->client->ZMATERIAL_STOCK_FOR_CART($params);
        $resp = json_decode(json_encode($response),true);
        return $resp;
    }

    public function Material_Details()
    {
       $params = array('WA_PRODUCT' => array());
       $request = $this->client->ZMATERIAL_DETAILS_DOWNLOAD($params);
       $resp = json_decode(json_encode($request),true);
        return $resp;
    }

    public function Cart_Price_List($stloc)
    {
       $params = array('STLOC'=>$stloc, 'WA_PRICE' => array());
       $request = $this->client->ZPRICE_LIST_FOR_CART($params);
       $resp = json_decode(json_encode($request),true);
        return $resp;
    }

    public function Sales_Office_Price_List($cond_type)
    {
       $params = array('CONDITIONTYPE'=>$cond_type, 'ZPRICE' => array(), 'ZPRICE_I' => array());
       $request = $this->client->ZSALESOFFICE_PRICE_LIST($params);
       $resp = json_decode(json_encode($request),true);
        return $resp;
    }




}
