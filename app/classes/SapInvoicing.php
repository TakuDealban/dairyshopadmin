<?php

namespace App\classes;

use SoapClient;

class SapInvoicing {


  protected $client;
  public function __construct($soapWSDL, $proxy_ip, $proxy_port,$proxy_login, $proxy_password, $login, $password )
  {
    $url = trim($soapWSDL, '"');
  

      $this->client = new SoapClient($soapWSDL,
                                array('proxy_host'=>$proxy_ip,
                                'proxy_port'=>$proxy_port,
                                'proxy_login'=>$proxy_login,
                                'proxy_password'=>$proxy_password,
                                'login'=>$login,
                                'password'=>$password,
                                 'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | 9
                            ));

                                                         
  }


  public function create_sales_order($params_array){
    $response = $this->client->ZCART_SALESORDER_CREATE($params_array);
    $resp = json_decode(json_encode($response), true);
    return $resp;
  }



}