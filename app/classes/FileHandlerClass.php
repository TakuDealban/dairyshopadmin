<?php

namespace App\classes;


use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Validator;

class file_handler_class {


    public static function handle_file(Request $request){

        if ($request->hasFile('image')){
            //Get filename with the extension
            Validator::make($request,
         [ 'image' => 'mimes:jpeg,jpg,png' ]);
 
            // $filenameWithExt = $request->file('image')->getClientOriginalName();
            // //Get Just Filename
 
            // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // //Get Just Ext
            // $extension = $request->file('image')->getClientOriginalExtension();
 
            // $fileNameToStore = $filename.'_'.time().'.'.$extension;
 
            // $request->file('image')->move('uploads/categories', $fileNameToStore);
 
         } else {
             $fileNameToStore = 'default.png';
         }

         return $fileNameToStore;
    }
}
