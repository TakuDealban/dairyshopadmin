<?php

use koolreport\widgets\google\BarChart;
use koolreport\widgets\google\ColumnChart;
use \koolreport\widgets\koolphp\Table;
?>

    <div>
 
        <?php
    $year = date("Y");
            BarChart::create(array(
                "title"=>"Top Ten SKUs [$year]",
                "width"=>"100%",
               // "height"=>"500px",
                "dataSource"=>$this->dataStore("vwtoptenytdskus"),
                "columns"=>array(
                    "name"=>array("label"=>"Product"),
                    "tot_sales"=>array("label"=>"Sales Value","type"=>"number", "decimals"=>2, "prefix"=>"$")
                ),
            ));
        

       

        ?>
    </div>
