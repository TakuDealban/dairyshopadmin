<?php
namespace App\Reports;

use koolreport\processes\Group;
use koolreport\processes\Limit;
use koolreport\processes\Sort;

class TopTenSkusYtd extends \koolreport\KoolReport
{
    use \koolreport\laravel\Friendship;
    use \koolreport\bootstrap4\Theme;
    // By adding above statement, you have claim the friendship between two frameworks
    // As a result, this report will be able to accessed all databases of Laravel
    // There are no need to define the settings() function anymore
    // while you can do so if you have other datasources rather than those
    // defined in Laravel.
    function setup () {
         $year = date("Y");
        //$year = 2020;
        $this->src("mysql") // use any of your preferred connection type in config/database.php
        ->query("SELECT tot_sales, name FROM vwtoptenytdskus where year = $year")
        ->pipe(new Limit(array(10)))
        ->pipe($this->dataStore("vwtoptenytdskus")); 
    }
}