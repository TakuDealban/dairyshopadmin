<?php

use koolreport\widgets\google\AreaChart;

?>

    <div>
 
        <?php

    $year = date("Y M");
            AreaChart::create(array(
                "title"=>"Daily Sales [$year]",
                "width"=>"100%",
                "dataSource"=>$this->dataStore("vwdailysales"),
                "columns"=>array(
                    "day_number"=>array("label"=>"Day", "type"=>"string"),
                    "day_sales"=>array("label"=>"Sales Value","type"=>"number", "decimals"=>2, "prefix"=>"$")
                ),
            ));
        

       

        ?>
    </div>
