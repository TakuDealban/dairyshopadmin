<?php

use koolreport\widgets\google\BarChart;
use koolreport\widgets\google\ColumnChart;
use \koolreport\widgets\koolphp\Table;
?>

    <div>
 
        <?php
    $year = date("Y");
            ColumnChart::create(array(
                "title"=>"MOM Sales [$year]",
                "width"=>"100%",
               // "height"=>"500px",
                "dataSource"=>$this->dataStore("vwmomsales"),
                "columns"=>array(
                    "month_name"=>array("label"=>"Month Name"),
                    "invoice_totals"=>array("label"=>"Sales Value","type"=>"number","decimals"=>2,"prefix"=>"$",
                    "style"=>function($row){
                        return "color:#ADBD37;fill-opactity:0.6";
                    })
                ),
            ));
        

       

        ?>
    </div>
