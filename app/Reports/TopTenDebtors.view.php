<?php

use koolreport\widgets\google\BarChart;
use koolreport\widgets\google\ColumnChart;
use \koolreport\widgets\koolphp\Table;
?>

    <div>
 
        <?php
    $year = date("Y");

    // $info = $this->dataStore("vwtoptendebtors");
    // $main_array = array();
    // foreach($info as $data){
    //     $new_array = array(
    //         "consumer_accout" => $data["ConsumerAccount"],
    //         ""
    //     );
    // }

    // $category_amount = array(
    //     array("category"=>"Books","sale"=>32000,"cost"=>20000,"profit"=>12000),
    //     array("category"=>"Accessories","sale"=>43000,"cost"=>36000,"profit"=>7000),
    //     array("category"=>"Phones","sale"=>54000,"cost"=>39000,"profit"=>15000),
    //     array("category"=>"Movies","sale"=>23000,"cost"=>18000,"profit"=>5000),
    //     array("category"=>"Others","sale"=>12000,"cost"=>6000,"profit"=>6000),
    // );
            ColumnChart::create(array(
                "title"=>"Top Ten Debtors [$year]",
                "width"=>"100%",
                "dataSource"=>$this->dataStore("vwtoptendebtors"),
                "columns"=>array(
                    "ConsumerAccount"=>array("label"=>"Account #"),
                    "ConsumerBalance"=>array("label"=>"Total Balance","type"=>"number", "decimals"=>2, "prefix"=>"$"),
                    "CurrentBal"=>array("label"=>"Current Balance","type"=>"number", "decimals"=>2, "prefix"=>"$"),
                    "unposted_receipts" => array("label"=>"Current Balance","type"=>"number", "decimals"=>2, "prefix"=>"$"),
                ),
            ));
        

       

        ?>
    </div>
