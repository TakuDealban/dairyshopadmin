<?php

namespace App\Imports;

use App\RecHeader;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomChunkSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class InvoiceImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new RecHeader([
            //
        ]);
    }

    public function collection(Collection $rows)
    {
       // dd($rows);
        foreach ($rows as $row) {
        RecHeader::updateOrCreate([
            'TransactionID' => $row['invoicenumber'],
        ], [
            'billing_no' => $row['sapbillingnumber'],
            'delivery_no' => $row['sapdeliverynumber'],
            'sync_date' => $row['syncdate'],
        ]);
        }
    }
}
