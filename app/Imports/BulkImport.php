<?php

namespace App\Imports;

use App\MainPriceListFromSap;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BulkImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $item)
    {
        return new MainPriceListFromSap([
            'st_loc' => $item['st_loc'],
            'plant' => $item['plant'],
            'sap_prod_code'  => $item["sap_prod_code"],
            'condition_type'  => $item["condition_type"],
           'price_list_type'  => $item["price_list_type"],
           'sales_org'  => $item["sales_org"],
           'dist_channel'  => $item["dist_channel"],
           'condition_number' => $item["condition_number"],                          
           'valid_from'  => $item["valid_from"],
           'valid_to'  => $item["valid_to"],
           'currency'  => $item["currency"],  
           'product_id' => $item["product_id"],                         
           'qty' => $item['qty'],
           'price'  => $item['price'],
           'price_usd' => $item["price_usd"],
           'created_by' => auth()->user()->id,
           'has_cond_number_in_list_I' => true
        ]);
    }
}



                       
  