<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::first();
        return view('company.index',['company' => $company]);
    }

    public function updateCompanyDemographics(Request $request){
        
       $update = Company::first()->update(array(
            'Name' => $request->input('Name'),
            'BPN' => $request->input('BPN'),
            'VATNo' => $request->input('VATNo'),
            'contact_email' => $request->input('contact_email'),
            'contact_tel' => $request->input('contact_tel'),
            'addr_one' => $request->input('addr_one'),
            'addr_two' => $request->input('addr_two'),
            'addr_three' => $request->input('addr_three'),
            'Vat_Rate' => $request->input('Vat_Rate'),
            'BaseCurrency' => $request->input('BaseCurrency'),
            'minimum_order_value' => $request->input('MinOrderValue'),
        ));
        return redirect()->back()->with('message','Company details have been updated succesfully..');

    }

    public function updateCompanyLogo(Request $request){
        $company = Company::first();
                $old_logo = $company->logo;
            $get_image = $this->handle_logo($request,$old_logo);
            $company->logo = $get_image;
            if($company->save()){
                return redirect()->back()->with('message','Company logo has been updated succesfully..');
            }

           
    }

    public function updatePaynow(Request $request)
    {
        $this->validate($request,[
        'Paynow_Integration_Key' => 'required',
        'Paynow_Integration_id' => 'required|numeric|min:1000'
        ]);
            $update = Company::first()->update(array(
                "Paynow_Integration_Key" => $request->input('Paynow_Integration_Key'),
                "Paynow_Integration_id" => $request->input('Paynow_Integration_id')
            ));
            return redirect()->back()->with('message','Company financial details have been updated succesfully..');
    }



     public function handle_logo(Request $request, $old_path)
    {
       
            if ($request->hasFile('image')){
                //Get filename with the extension
                $this->validate($request,
             [ 'image' => 'mimes:jpeg,jpg,png,JPG' ]);
     
                $filenameWithExt = $request->file('image')->getClientOriginalName();
                //Get Just Filename
     
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                //Get Just Ext
                $extension = $request->file('image')->getClientOriginalExtension();
     
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
     
                $request->file('image')->move('uploads/company', $fileNameToStore);
     
             } else {
                 $fileNameToStore = $old_path;
             }
    
             return $fileNameToStore;
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
