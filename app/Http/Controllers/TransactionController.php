<?php

namespace App\Http\Controllers;

use App\BillingDetail;
use App\classes\SMS;
use App\PaymentRequest;
use App\RecHeader;
use App\RecItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{
    public function showTransactions()
    {
       $Transactions = RecHeader::all();
       return view('transactions.index')->with(compact('Transactions'));
    }

    public function invoicesToPush()
    {
       $Transactions = RecHeader::whereNull('billing_no')
                    ->orWhereNull('delivery_no')->get();
       return view('transactions.index')->with(compact('Transactions'));
    }

    public function showAttewmpts()
    {
        $Transactions = PaymentRequest::where('trans_id', '=', null)->where('status', '<>', 'paid')->get();
        return view('transactions.attempts')->with(compact('Transactions'));
    }

    public function Customers()
    {
        $Transactions = BillingDetail::distinct('phone')->get();
        
        return view('transactions.customers')->with(compact('Transactions'));
    }

     public function viewUpdateInvoiceStatus($invoice_number)
    {
       
       return view('transactions.updateStatus')->with(compact('invoice_number'));
    }

    public function processInvoiceStatus(Request $request)
    {

       $this->validate($request,[
            'order_status' => 'required',
            'optradio' => 'required'

       ]);

       $comment_text = $request->input('comment');
       $comms_method = $request->input("optradio");
       $invoice_number = $request->input("invoice_number");
      // if($comment == null){
           $comment = "Dear Customer your Invoice with reference $invoice_number has been updated to status ".$request->input("order_status").". $comment_text.";
      // }
           
      $log_string = "Time: " . date("Y-m-d H:i:s") . " : Log : ".json_encode($request->all(),true);
      Log::channel('command')->info($log_string);

       $update = RecHeader::where("TransactionID",$invoice_number)->first();
       $phone = $update->Phone;
       $update->order_status = $request->input("order_status");
       $update->save();
         

       if($comms_method == "sms" || $comms_method == "both"){
            //send sms
            $to = $phone;
            $from = "Dairibord";
            $msg = $comment;
        $sms = new SMS($to, $from, $msg);
        $send = $sms->SendSms();
        $sms_logger = json_encode($send, true);
        $log_string = "Time: " . date("Y-m-d H:i:s") . " : SMS Log : $sms_logger";
        Log::channel('command')->info($log_string);
       }

       return redirect()->route('showTransactions')->with('message','All transactions have been updated successfully..');

    }

    public function TransactionDetails($tran_ref)
    {
        $order_details = RecHeader::where('TransactionID',$tran_ref)->first();
        if($order_details != null){
            $order_number = $order_details->OrderID;
            $trans_products = RecItems::where('order_number', $order_number)->get();
            return view('transactions.invoiceprod')->with(compact('trans_products','order_details'));
        }
        
      
    }

  

}
