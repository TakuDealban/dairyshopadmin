<?php

namespace App\Http\Controllers;

use App\BillingDetail;
use App\classes\SapInvoicing;
use App\Exports\InvoicesExport;
use App\Imports\InvoiceImport;
use App\RecHeader;
use App\RecItems;
use App\SapWebservicesconfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Stmt\TryCatch;
use SoapFault;

class SapInvoicingController extends Controller
{
    /**
     * get invpoices to sync to SAP.
     *
     * @param  no_params
     * @return \Illuminate\Http\Response
     */
    public function GetInvoicesToSync() 
    {
        $invoices_to_sync = RecHeader::whereNull('billing_no')
                                        ->orWhereNull('delivery_no')->get();
                                        //dd($invoices_to_sync);
        return $invoices_to_sync;
    }

    public function syncInvoicesToSap(){

        try {
            //code...
       
        $all_invoices = $this->GetInvoicesToSync();
$status = "fail";
$p = 0;

        if (!empty($all_invoices) && count($all_invoices) > 0) {
            foreach ($all_invoices as $invoice) {
                $inv_number = $invoice->TransactionID;
                $order_reference = $invoice->OrderID;

                $invoice_details = RecItems::where('order_number',$order_reference)->get();
                $billing_details = BillingDetail::where('order_ref',$order_reference)->first();

                $i = 0;
                $Val = array();
                foreach ($invoice_details as $invoice_detail) {
                   
                    $material_number = $invoice_detail->rec_item_product->sap_product_code;
                    $po_number = $inv_number;
                    $qty = $invoice_detail->quantity;
                    $Uom = $invoice_detail->rec_item_product->UOM;
                    
                    //$SoldToParty = $billing_details->first_name;
                    //$shiptoparty = $billing_details->id;
                    // $SoldToParty = "TK";
                    $SoldToParty = 7314;
                    $shiptoparty = 7314;
        
        
                    $ItemNum = $i + 10;
        
                    $Val[] = array(
                        'BATCH' => "D100",
                        'DIST_CHAN' => "02",
                        'DIVISION' => "01",
                        'ITM_NUMBER' => $ItemNum,
                        'MATERIAL' => $material_number,
                        'ORDER_TYPE' => "O100",
                        'PO_NUMBER' => $po_number,
                        'QUANTITY' => $qty,
                        'SALES_OFF' => "D1",
                        'SALES_ORG' => "DZL",
                        'SOLD_TO_PARTY' => $SoldToParty,
                        'SHIP_TO_PARTY' => $shiptoparty,
                        'UOM' => $Uom
                    );
                    $i+=10;
                }
                $params = array(
                    'ORDER_TYPE' => "O100",
                    'ITEMTAB' => array('item' => $Val)
                );
       
                $invoice_sap_instance  =  $this->createSapObject();
                $sync_invoice = $invoice_sap_instance->create_sales_order($params);


                Log::channel('command')->info("Invoice Sync Details: ".json_encode($sync_invoice));
                $sapInvoiceNumber = $sync_invoice["BILLING_NO"];
                $sapDeliveryNumber = $sync_invoice["DELIVERY_NO"];
                if((!empty($sapDeliveryNumber) && $sapDeliveryNumber != "" & $sapDeliveryNumber != null)){
                        //update header
                        $recHeaderToUpdate = RecHeader::where('TransactionID',$inv_number)->first();
                        if($recHeaderToUpdate != null){
                            $recHeaderToUpdate->billing_no = $sapInvoiceNumber;
                            $recHeaderToUpdate->delivery_no = $sapDeliveryNumber;
                            $recHeaderToUpdate->sync_date = date("Y-m-d H:i:s");
                            if($recHeaderToUpdate->save()){
                             $status = "ok";   
                             $p += 1;
                            }
                            
                        }
                }

            }
            if($status == "ok"){
                $file_name = "UploadedInvoices".date("ymdhis").".xlsx";
                return  Excel::download(new InvoicesExport, $file_name);

                return redirect()->back()->with('message', 'A total of '.$p.' invoices have been uploaded successfully .. ');
            }
            else{
                return redirect()->back()->with('error', 'Failed to upload invoices. '.$sync_invoice["ORD_ERROR"]);
            }
    }
    else{
        $message = "No Invoices to Sync";
        return redirect()->back()->with('error', $message);
    }
} catch (SoapFault $th) {
    return redirect()->back()->with('error', $th->getmessage());

}
  
}

public function import_from_excel_invoice_view()
{
  return view('transactions.import_sap_register');
}  

public function load_billing_documents(Request $request)
{
    $this->validate($request,[
        'sap_register' => 'required|mimes:xls,xlsx'
 ]);

// $file = $request->file('price_lists');
Excel::import(new InvoiceImport,request()->file('sap_register'));

$sloc = "all";
return  redirect()->back()->with('message', 'Invoices updated successfully.');

}

private function createSapObject(){
            $sapWebService = SapWebservicesconfig::first();
            if($sapWebService == null){
            return redirect()->back()->with('error', "Please configure the webservice parameters.."); 
            }

            $soapWSDL = $sapWebService->url;
            $proxy_ip = $sapWebService->proxy_ip;
            $proxy_port = $sapWebService->proxy_port;
            $proxy_login = $sapWebService->proxy_login;
            $proxy_passkey = $sapWebService->proxy_password;
            $login = $sapWebService->login;
            $password = $sapWebService->password;

            $sap_integration = new SapInvoicing($soapWSDL,$proxy_ip,$proxy_port,$proxy_login,$proxy_passkey,$login,$password);
            return $sap_integration;
        
        }

}
