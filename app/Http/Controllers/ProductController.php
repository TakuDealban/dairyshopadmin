<?php

namespace App\Http\Controllers;

use App\Category;
use App\classes\ProductLogs;
use App\Imports\BulkImport;
use App\MainPriceListFromSap;
use App\PickUpPoint;
use App\PriceListControl;
use App\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index',["products" =>  $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('products.create',
        ['categories' => $categories]);
    }

    /**
     * Redirects tp the import products view.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload_product_view()
    {
        $pick_up_points = PickUpPoint::all();
        return view('products.sapproductsrequest',[
                'pick_up_points' => $pick_up_points
        ]);
    }

  public function import_from_excel_product_view()
  {
    return view('products.import_from_excel');
  }  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:products,name',
            'product_code' => 'required|unique:products,product_code',
            'stock_on_hand' => 'numeric',
            'price' => 'numeric'
        ]);
      
        $input = $request->all();
        $image = $this->handle_image($request);
        $input['imagePath'] = $image;
        $input['created_by'] = auth()->user()->id;
        $create_product = Product::create($input);
            $last_id = $create_product->id;
            $product_logs = ProductLogs::log_product_movement("New Product", "New Product - ".$input["product_code"],$last_id,$input["stock_on_hand"],$input["stock_on_hand"],$input["stock_on_hand"],"New Product");

        return redirect()->route('products')->with('message','Product has been created succesfully..');
    }

    public function load_price_lists(Request $request){
        $this->validate($request,[
               'price_lists' => 'required|mimes:xls,xlsx'
        ]);

        $delete =  MainPriceListFromSap::Truncate();

       // $file = $request->file('price_lists');
        Excel::import(new BulkImport,request()->file('price_lists'));

            $sloc = "all";
            return  redirect()->route('vwStorageLocProducts',$sloc)->with('message', 'Products masterfile, Product Pricing, Product quanties and Product categories have been successfully uploaded from SAP');

    }

    public function excel_loop($transData){

        foreach($transData->toArray() as $key => $item){
           
            if($item["sap_prod_code"] == "" || $item["sap_prod_code"] == null || empty($item["sap_prod_code"]))
            {
                continue;
            }
           
            $price = 0.00;
            $has_link = true;

            $product = Product::where('sap_product_code',$item["sap_prod_code"])->first();
            if($product == null){
                $product_id = 0;
                continue;
            }
            else{
                $product_id = $product->id;
            }
            
             $STLOC = $item["st_loc"];

            $price = (float)round($item["price"],2);
            $usd_price = (float)round($price / 80,2);

                MainPriceListFromSap::updateOrCreate(
                    [
                       
                        'st_loc' => $STLOC,
                        'plant' => $item['plant'],
                        'sap_prod_code'  => $item["sap_prod_code"]
                    ],
                    [
                       'condition_type'  => $item["condition_type"],
                       'price_list_type'  => $item["price_list_type"],
                       'sales_org'  => $item["sales_org"],
                       'dist_channel'  => $item["dist_channel"],
                       'condition_number' => $item["condition_number"],                          
                       'valid_from'  => $item["valid_from"],
                       'valid_to'  => $item["valid_to"],
                       'currency'  => $item["currency"],  
                       'product_id' => $product_id,                         
                       'qty' => $item['qty'],
                       'price'  => $price,
                       'price_usd' => $usd_price,
                       'created_by' => auth()->user()->id,
                       'has_cond_number_in_list_I' => $has_link
                    ]
                );
            //  $logger = new productLogs();
            //   $logger->log_product_changes($product['id'],0,"SELL",Enumerations::SOLD,$sold_qty,$old_qty,$new_qty,"SELL");
        } //end of trans
    }


    public function handle_image(Request $request){
        if ($request->hasFile('image')){
            //Get filename with the extension
            $this->validate($request,
         [ 'image' => 'mimes:jpeg,jpg,png,JPG' ]);
 
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //Get Just Filename
 
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get Just Ext
            $extension = $request->file('image')->getClientOriginalExtension();
 
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
 
            $request->file('image')->move('uploads/products', $fileNameToStore);
 
         } else {
             $fileNameToStore = 'default.png';
         }

         return $fileNameToStore;
    }

    public function handle_update_image(Request $request, $old_image_path)
    {
        if ($request->hasFile('image')){
            //Get filename with the extension
            $this->validate($request,
         [ 'image' => 'mimes:jpeg,jpg,png,JPG' ]);
 
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //Get Just Filename
 
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get Just Ext
            $extension = $request->file('image')->getClientOriginalExtension();
 
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
 
            $request->file('image')->move('uploads/products', $fileNameToStore);
 
         } else {
             $fileNameToStore = $old_image_path;
         }

         return $fileNameToStore;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($prod)
    {
        $product = Product::find($prod);
        $categories = Category::all();
        return view('products.edit')->with(compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $prod)
    {
        $this->validate($request,[
            'name' => 'required|unique:products,name,'.$prod,
            'product_code' => 'required|unique:products,product_code,'.$prod,
            'stock_on_hand' => 'numeric',
            'price' => 'numeric'
        ]);

        $prod_info = Product::findorfail($prod);
        $input = $request->all();
        $image = $this->handle_update_image($request,$prod_info->imagePath);
        $input['imagePath'] = $image;
        $input['created_by'] = auth()->user()->id;
        $update_product = $prod_info->update($input);
            $last_id = $prod_info->id;
           ProductLogs::log_product_movement("Update Product", "Update Product - ".$input["product_code"],$last_id,$input["stock_on_hand"],$input["stock_on_hand"],$input["stock_on_hand"],"Update Product");

        return redirect()->route('products')->with('message','Product has been updated succesfully..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
