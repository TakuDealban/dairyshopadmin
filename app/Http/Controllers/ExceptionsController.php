<?php

namespace App\Http\Controllers;

use App\BillingDetail;
use App\classes\ProductLogs;
use App\Classes\SMS;
use App\PaymentRequest;
use App\Product;
use App\RecHeader;
use App\RecItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExceptionsController extends Controller
{
    //
    public function ListFailedTransactions()
    {
        $failed_receipts = RecItems::select('order_number', DB::raw('count(*) as total'))
                            ->where('status','Paid')
                            ->whereNotIn('order_number', DB::table('recheader')->pluck('OrderID'))
                            ->groupBy('order_number')
                            ->get();
        return view('exceptions.receipts',[
            'failed_rec' => $failed_receipts
        ]);
        
    }

    public function FixFailedTrans($order_number)
    {
       $trans_id = str_replace('online_dairy_','Inv',$order_number);
       $order_details = PaymentRequest::where('request_reference',$order_number)->first();
       $shipment_charge = 0.00;
            $destination = "Source";

       $rec_header = new RecHeader();
       $rec_header->TransactionID = $trans_id;
       $rec_header->OrderID = $order_number;
       $rec_header->AmountPaid = $order_details->tot_bill;
       $rec_header->Phone = $order_details->phone;
       $rec_header->Email = $order_details->email;
       $rec_header->shipment_charge = $shipment_charge;
       $rec_header->destination_area = $destination;
       $rec_header->storage_location = "HQ";
       if($rec_header->save()){
           //send sms
        $rec_items = RecItems::where('order_number',$order_number)->get();
           $this->reduceqty($rec_items);
            $this->store_cookie_in_db($order_number,$order_details);
           $msg = "Dear customer, your order has been updated. Your order number is $trans_id. Thank you for shopping with Dairibord.";
           $sendSMS = new SMS($order_details->phone,'Dairibord', $msg);
           $sendSMS->SendSms();
           return redirect()->back()->with('message', 'Receipt has been created successfully. The new Invoice number is '.$trans_id);
       }
    }

    public function reduceqty($products)
{
    foreach($products as $product)
    {
            $product_id = $product->prodid;
         $prod_info =  Product::find($product_id);
            $old_qty = $prod_info->stock_on_hand;
            $newQty = $old_qty - $product->quantity;
            $prod_info->stock_on_hand = $newQty;
            if($prod_info->save()){
                $log = ProductLogs::log_product_movement("Sale","Sold",$product_id,$product->quantity,$old_qty,$newQty,"Sale");
            }

    }
}

public function store_cookie_in_db($order_ref, $payment_request){
    //store cookie info in DB
    $billing_details = new BillingDetail();
    $billing_details->first_name = $payment_request->email;
    $billing_details->last_name = $payment_request->email;
    $billing_details->company = $payment_request->email;
    $billing_details->email = $payment_request->email;
    $billing_details->phone = $payment_request->phone;
    $billing_details->country = $payment_request->email;
    $billing_details->address = $payment_request->email;
    $billing_details->apartment = $payment_request->email;
    $billing_details->town = $payment_request->email;
    $billing_details->state = $payment_request->email;
    $billing_details->zip = $payment_request->phone;
    $billing_details->order_ref = $order_ref;
    
    $billing_details->save();
  
    
    }
}
