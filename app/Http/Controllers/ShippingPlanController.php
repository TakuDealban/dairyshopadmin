<?php

namespace App\Http\Controllers;

use App\ExchangeRate;
use App\ShippingPlan;
use Illuminate\Http\Request;

class ShippingPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = ShippingPlan::get();
        return view('shipment.index',['plans' => $plans]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currencies = ExchangeRate::all();
        return view('shipment.create')->with(compact('currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'location_to' => 'required|unique:shipping_plans,location_to',
            'shipment_charge' => 'required',
            'shipment_charge_currency' => 'required'
        ]);

        $input = $request->all();
        $input['created_by'] = auth()->user()->id;
        ShippingPlan::create($input);

        return redirect()->route('shippingplans')->with('message','Plan has been created succesfully..');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShippingPlan  $shippingPlan
     * @return \Illuminate\Http\Response
     */
    public function show(ShippingPlan $shippingPlan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShippingPlan  $shippingPlan
     * @return \Illuminate\Http\Response
     */
    public function edit($shipping_id)
    {
        $plan = ShippingPlan::findorfail($shipping_id);
        $currencies = ExchangeRate::all();
        return view('shipment.edit')->with(compact('plan','currencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShippingPlan  $shippingPlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $plan_id)
    {
        $this->validate($request,[
            'location_to' => 'required|unique:shipping_plans,location_to,'.$plan_id,
            'shipment_charge' => 'required',
            'shipment_charge_currency' => 'required'
        ]);
        
        $plan_info = ShippingPlan::findorfail($plan_id);
        $input = $request->all();
        $input['created_by'] = auth()->user()->id;
        $plan_info->update($input);

        return redirect()->route('shippingplans')->with('message','Plan has been update succesfully..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShippingPlan  $shippingPlan
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShippingPlan $shippingPlan)
    {
        //
    }
}
