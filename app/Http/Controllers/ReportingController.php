<?php

namespace App\Http\Controllers;

use App\ExchangeRateLog;
use App\ProductAudit;
use App\RecHeader;
use App\Reports\DailySales;
use App\Reports\MomSales;
use App\Reports\TopTenSkusYtd;
use Illuminate\Http\Request;

class ReportingController extends Controller
{
    public function index () {
        $report = new MomSales();
        $report->run();

        $topTenSkus = new TopTenSkusYtd();
        $topTenSkus->run();

        $dailySales = new DailySales();
        $dailySales->run();

        $Transactions = RecHeader::orderby('DateCreated','Desc')->take(5)->get();

        $exchangeRate = ExchangeRateLog::orderby('created_at','Desc')->take(10)->get();

        $productMovement = ProductAudit::orderby('created_at','Desc')->take(10)->get();

        return view("dashboard",["report"=>$report,"topTenSkus" => $topTenSkus, 
                            "dailySales" => $dailySales, "Transactions" => $Transactions,
                            "exchangeRate" => $exchangeRate,  "productMovement" => $productMovement
                            ]);
}


}
