<?php

namespace App\Http\Controllers;

use App\ExchangeRate;
use App\ExchangeRateLog;
use App\MainPriceListFromSap;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ExchangeRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = ExchangeRate::all();
        return view('currencies.index',[
            'currencies' => $currencies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $distinct_currencies = ExchangeRate::select('forexCurrency')->groupBy('forexCurrency')->get();
        $currencies = array();
        foreach($distinct_currencies as $curr)
        {
                array_push($currencies,$curr["forexCurrency"]);
        }
         $comma_sep_curr = implode(" , ",$currencies);
        //  dd($comma_sep_curr);
        return view('currencies.create',[
             'currencies' => $comma_sep_curr
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'forexcurrency' => 'required|unique:exchange_rates,baseCurrency,NULL,NULL,forexCurrency,'.$request->input('basecurrency'),
            'basecurrency' => 'required',
            'exchangeRate' => 'required|numeric'
        ],
        ['forexcurrency.unique' => 'Pay Type combination of '.$request->input('forexcurrency').' and '.$request->input('basecurrency').'  already exists.']
    );

    $saveCurrency = new ExchangeRate();
    $saveCurrency->baseCurrency = $request->input('forexcurrency');
    $saveCurrency->forexCurrency = $request->input('basecurrency');
    $saveCurrency->exchangeRate = $request->input('exchangeRate'); 
    $saveCurrency->forex_description = $request->input('currency_description');
    $saveCurrency->uuid =  Str::random(32);
    $saveCurrency->created_by = auth()->user()->id;
    $saveCurrency->updated_by = auth()->user()->id;
    $saveCurrency->save();

    $lastID = $saveCurrency->id;

    $saveLogs = new ExchangeRateLog();
    $saveLogs->exchange_rate_id = $saveCurrency->id;
    $saveLogs->comment = "Creation of combination for 1 ". $request->input('forexcurrency') ." to  ".$request->input('exchangeRate')."  ". $request->input('basecurrency');
    $saveLogs->exchange_rate_from = 0;
    $saveLogs->exchange_rate_to =  $request->input('exchangeRate');
    $saveLogs->created_by = auth()->user()->id;
     $saveLogs->save();

    if ($saveLogs->save()) {
        return redirect()->route('exchangerates')->with('message','Pay Type has been saved successfully');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExchangeRate  $exchangeRate
     * @return \Illuminate\Http\Response
     */
    public function show(ExchangeRate $exchangeRate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExchangeRate  $exchangeRate
     * @return \Illuminate\Http\Response
     */
    public function edit($exchangeRateId)
    {
        $Currency = ExchangeRate::findorfail($exchangeRateId);
        return view('currencies.edit')->with(compact('Currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExchangeRate  $exchangeRate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $exRateId)
    {
        $this->validate($request,
        [
            'forexCurrency' => 'required|unique:exchange_rates,forexCurrency,NULL,NULL,baseCurrency,'.$request->input('baseCurrency').',id,!'.$exRateId,
             'exchangeRate' => 'required|numeric'
        ],
        ['forexCurrency.unique' => 'Pay Type combination of '.$request->input('forexCurrency').' and '.$request->input('baseCurrency').'  already exists.']
    );

    $saveCurrency = ExchangeRate::findorfail($exRateId);
    $old_rate = $saveCurrency->exchangeRate;
    $saveCurrency->baseCurrency = $request->input('baseCurrency');
    $saveCurrency->forexCurrency = $request->input('forexCurrency');
    $saveCurrency->exchangeRate = $request->input('exchangeRate'); 
    $saveCurrency->forex_description = $request->input('forex_description');
   //$saveCurrency->uuid =  Str::random(32);
    $saveCurrency->created_by = auth()->user()->id;
    $saveCurrency->updated_by = auth()->user()->id;
    $saveCurrency->save();

    $lastID = $saveCurrency->id;

    $saveLogs = new ExchangeRateLog();
    $saveLogs->exchange_rate_id = $saveCurrency->id;
    $saveLogs->comment = "Update of exchange rate for 1 ". $request->input('baseCurrency') ." to  ".$request->input('exchangeRate')."  ". $request->input('forexCurrency');
    $saveLogs->exchange_rate_from = $old_rate;
    $saveLogs->exchange_rate_to =  $request->input('exchangeRate');
    $saveLogs->created_by = auth()->user()->id;
     $saveLogs->save();

    if ($saveLogs->save()) {
        //update  prices for all products with the new rate only if base currency = ZWL and foreign cur  - USD
        $this->updateProductsUsdPrice($request);
        return redirect()->route('exchangerates')->with('message','Pay Type has been updated successfully');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExchangeRate  $exchangeRate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExchangeRate $exchangeRate)
    {
        //
    }

    private  function updateProductsUsdPrice(Request $request)
    {
        $exchangeRate = $request->input('exchangeRate');
        
        if($request->input('baseCurrency') == "ZWL" && $request->input('forexCurrency') == "USD")
        {
            $products = MainPriceListFromSap::all();
            foreach($products as $product){
                $id = $product->id;
                $productInfo = MainPriceListFromSap::findorfail($id);
                $productInfo->price_usd = $exchangeRate * $productInfo->price;
                $productInfo->save();
            }
        }

    }
}
