<?php

namespace App\Http\Controllers;

use App\MainPriceListFromSap;
use App\PickUpPoint;
use App\SapWebservicesconfig;
use Illuminate\Http\Request;

class SAPWebServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $webservices = SapWebservicesconfig::first();
        $pickuppoints = PickUpPoint::all();
        return view('sap.index',[
                        'webservices' => $webservices,
                        'pickuppoints' => $pickuppoints
                    ]);
    }

    public function newpickup()
    {
       
        return view('sap.ppcreate');
    }

    public function vwStorageLocations()
    {
        $pickuppoints = PickUpPoint::all();
       return view('sap.stlocs',[
        'pickuppoints' => $pickuppoints
       ]);
    }

    public function vwStorageLocProducts($sloc)
    {
        if($sloc == "all"){
            $products = MainPriceListFromSap::all();
        }
        else{
            $products = MainPriceListFromSap::where('st_loc', $sloc)->get();
        }
     
       return view('sap.stlocproducts',[
           'products' => $products
       ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveWebServiceSettings(Request $request)
    {

        $header = SapWebservicesconfig::first();
        if($header == null){
            $sapWebHeader = new SapWebservicesconfig();
        }
       else{
           $sapWebHeader = $header;
       }
        $sapWebHeader->url = $request->input('url');
        $sapWebHeader->proxy_ip = $request->input('proxy_ip');
        $sapWebHeader->proxy_port = $request->input('proxy_port');
        $sapWebHeader->proxy_login = $request->input('proxy_login');
        $sapWebHeader->proxy_password = $request->input('proxy_password');
        $sapWebHeader->login = $request->input('login');
        $sapWebHeader->password = $request->input('password');
        $sapWebHeader->created_by = auth()->user()->id;
        if($sapWebHeader->save()){
            return redirect()->back()->with('message','Web Service settings have been uploaded succesfully..');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function savePickUpPointsSettings(Request $request)
    {
        

        $pickuppoints = new PickUpPoint();
        $pickuppoints->storage_location = $request->input('storage_location');
        $pickuppoints->plant = $request->input('plant');
        $pickuppoints->order_type = $request->input('order_type');
        $pickuppoints->pick_up_point = $request->input('pick_up_point');
        $pickuppoints->city = $request->input('city');
        $pickuppoints->usd_customer_account = $request->input('usd_customer_account');
        $pickuppoints->zwl_customer_account = $request->input('zwl_customer_account');
        $pickuppoints->created_by = auth()->user()->id;

        if($pickuppoints->save()){
            return redirect()->back()->with('message','Plant and storage location details have been uploaded succesfully..');
        }
    }

    public function updatePickUpPointsSettings(Request $request, $id)
    {
        $pickuppoints = PickUpPoint::findorfail($id);
        $pickuppoints->storage_location = $request->input('storage_location');
        $pickuppoints->plant = $request->input('plant');
        $pickuppoints->order_type = $request->input('order_type');
        $pickuppoints->pick_up_point = $request->input('pick_up_point');
        $pickuppoints->city = $request->input('city');
        $pickuppoints->usd_customer_account = $request->input('usd_customer_account');
        $pickuppoints->zwl_customer_account = $request->input('zwl_customer_account');
        $pickuppoints->created_by = auth()->user()->id;

        if($pickuppoints->save()){
            return redirect()->back()->with('message','Plant and storage location details have been updated succesfully..');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pickuppoint_info = PickUpPoint::findorfail($id);
        return view('sap.ppedit',[
            'pickuppoints' => $pickuppoint_info
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
