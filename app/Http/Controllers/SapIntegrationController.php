<?php

namespace App\Http\Controllers;

use App\Category;
use App\classes\SapIntegration;
use App\ExchangeRate;
use App\Exports\BulkExport;
use App\MainPriceListFromSap;
use App\PriceListControl;
use App\Product;
use App\SalesOfficePriceList;
use App\SapCartItem;
use App\SapWebservicesconfig;
use Exception;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Stmt\TryCatch;
use SoapFault;

class SapIntegrationController extends Controller
{
    //
     /**
   * Get all materials for cart and returns the following sample response
   * 0 => array:5
    *   "WERKS" => "S310" //plant
    *   "LGORT" => "S106"
    *   "MATNR" => "000000000000122165"
    *    "MAKTX" => "Yummy Banana Yoghurt  1x10x150 ml"
    *    "LABST" => "1000.0"
   */

    // public function show_materials(){ // update qty 
    //     $year = 2020;
    //         $sap_integration = new SapIntegration();
    //         $materials = $sap_integration->Material_FOR_CART($year);
    //         $cart_stock = $materials['ZSTOCK']["item"];
    //         foreach($cart_stock as $items){
    //                 SapCartItem::updateOrCreate([
    //                     'sap_product_code' => $items["MATNR"],
    //                 ],
    //                 [
    //                     'plant' => $items["WERKS"],
    //                     'storage_loc' => $items["LGORT"],
    //                     'sap_material_desc' => $items["MAKTX"],
    //                     'quantity' => $items["LABST"],
    //                     'created_by' => auth()->user()->id
    //                 ]
    //             );

    //           $update =  Product::where('sap_product_code',$items["MATNR"])->update([
    //                     'stock_on_hand' => $items["LABST"]
    //                 ]);

    //             if($update){
    //                 SapCartItem::where('sap_product_code', $items["MATNR"])->update(
    //                     ['in_master' => 1]
    //                 );
    //             }
    //         }
    // }

    /**
     * Get all materials with details  materials
     * Response:
     * "MATNR" => "000000000000132018"
     * "MAKTX" => "Steri-milk 1x 20 x 300ml shrinkwrapped"
     * "TAXM1" => "0"
     * "UMREZ" => "20"
     *  "UMREN" => "1"
     *  "MEINH" => "CS"
     *  "MVGR1" => "01"
     *  "BEZEI" => "Long Life Milk"
      ]
      */
    public function get_all_materials_with_details(Request $request){ 
        try {
          
            $this->validate($request,[
                'storage_location' => 'required'
            ]);

            $sap_integration =  $this->createSapObject();
            $materials = $sap_integration->Material_Details();
            // dd($materials);
        //     if(count($materials["WA_PRODUCT"]) > 0) 
        //     {
        //         $materials_list = $materials["WA_PRODUCT"]["item"];
      
           
         
        //     foreach($materials_list as $list){
        //         $materialsCategoryCode = $list["MVGR1"];
        //         $category = Category::where("sap_category_code", $materialsCategoryCode)
        //                                 ->orWhere('name',$list["BEZEI"])->first();
        //         if($category == null){
        //             $category_new  = new Category();
        //             $category_new->name = $list["BEZEI"];
        //             $category_new->description = $list["BEZEI"];
        //             $category_new->sap_category_code = $materialsCategoryCode;
        //             $category_new->created_by = auth()->user()->id;
        //             if($category_new->save()){
        //                 $category_id = $category_new->id;
        //             }
        //         }
        //         else{
        //             $category_id = $category->id;
        //         }


        //             $sap_material_number = $list['MATNR'];
        //            Product::updateOrCreate(
        //                 [
        //                 'sap_product_code' => $sap_material_number,
                        
        //                 ],
        //                 ['UOM' => $list['MEINH'],
        //                 'name' => $list['MAKTX'],
        //                 'product_code' => ltrim($sap_material_number, '0'),
        //                 'VAT_CODE' => $list['TAXM1'] == '1' ? 'A' : 'B',                        
        //                 'price' => 0,
        //                 //'imagePath' => 'default.png',
        //                 'category_description' => $list['BEZEI'],
        //                 'category_id' => $category_id,
        //                 'created_by' => auth()->user()->id
        //                 ]);

        //                 //update stock
        //     }
        // }

      
           $this->cart_price_list($request);

           $file_name = "File_Bulk_Dump_".date("ymdhis").".xlsx";
           return  Excel::download(new BulkExport, $file_name);

        } catch (SoapFault $th) {
            // return redirect()->back()->with('error', $th->getmessage());
          dd($th->getmessage());
        }
      
        //  return  redirect()->route('vwStorageLocProducts',$request->input('storage_location'))->with('message', 'Products masterfile, Product Pricing, Product quanties and Product categories have been successfully uploaded from SAP');
       
    }

    // public function UploadPrices()
    // {
    //    $this->cart_price_list();
    //    $this->sales_office_price_list();
    //    return  redirect()->route('products')->with('message', 'products pricing has been uploaded from SAP');
    // }

    /**
     * 
     * [ZPRICE] RETURNS
     * "KNUMH" => "0000394398"
     * "KSCHL" => "ZPRO"
     * "PLTYP" => "05"
     * "VKORG" => "DZL"
     * "VTWEG" => "05"
     * "DATAB" => "2020-05-29"
     * "DATBI" => "9999-12-31"
     * "WAERK" => "USD"
     * "MATNR" => "000000000000122164"
     * "KBETR" => "0.0"
     * "MAKTX" => ""   
     */
    
    public function cart_price_list(Request $request)
    {
     
        try {

          $STLOC = $request->input('storage_location');

          $sap_object = $this->createSapObject();
             $materials = $sap_object->cart_price_list($STLOC);

           
         
            if(!empty($materials["WA_PRICE"]))
            {


            
            $cart_prices = $materials["WA_PRICE"]["item"];
            //dd(count($cart_prices));
          //  dd(($cart_prices));
            $cartPriceCounter = count($cart_prices);

           if($cartPriceCounter > 1){
            MainPriceListFromSap::truncate();
            
            if(@$cart_prices["MATNR"] != "" || @$cart_prices["MATNR"] != null){
                $item = $cart_prices;
             $this->upload_single_product($item,$STLOC);
            }
        else{
            foreach($cart_prices as $item){
     
                if($item["MATNR"] == "" || $item["MATNR"] == null || empty($item["MATNR"]))
                {
                    continue;
                }
                $price_info = PriceListControl::where('condition_number', $item["KNUMH"])->first();
                $price = 0.00;
                $has_link = true;

                $product = Product::where('sap_product_code',$item["MATNR"])->first();
                if($product == null){
                    $product_id = 0;
                }
                else{
                    $product_id = $product->id;
                }
                
                $base_cur = config('app.BASE_CURRENCY');
                $usd_exc_rates = ExchangeRate::where('baseCurrency',$base_cur)->where('forexCurrency','USD')->first();
                if($usd_exc_rates != null){
                    $rate = round($usd_exc_rates->exchangeRate,4);
                }
                else{
                    $rate = round(1/80,4);
                }
                $price = (float)round($item["KBETR"],2);
                $usd_price = (float)round($price * $rate,2);

                    MainPriceListFromSap::updateOrCreate(
                        [
                           
                            'st_loc' => $STLOC,
                            'plant' => $item['WERKS'],
                            'sap_prod_code'  => $item["MATNR"]
                        ],
                        [
                           'condition_type'  => $item["KSCHL"],
                           'price_list_type'  => $item["PLTYP"],
                           'sales_org'  => $item["VKORG"],
                           'dist_channel'  => $item["VTWEG"],
                           'condition_number' => $item["KNUMH"],                          
                           'valid_from'  => $item["DATAB"],
                           'valid_to'  => $item["DATBI"],
                           'currency'  => $item["WAERK"],  
                           'product_id' => $product_id,                         
                           'qty' => $item['LABST'],
                           'price'  => $price,
                           'price_usd' => $usd_price,
                           'created_by' => auth()->user()->id,
                           'has_cond_number_in_list_I' => $has_link
                        ]
                    );
    
                                   
                }
            }
        }
    }

        } catch (SoapFault $th) {
            return redirect()->route('products')->with('error', $th->getmessage());
        }
        
        // $cart_header = $materials["ZPRICE_I"]["item"];
        // $sort_header = $this->Create_Price_List_Header($cart_header);

        
    }

    private function export_file() 
    {
        return Excel::download(new BulkExport, 'bulkData.xlsx');
    }

    private function upload_single_product($item,$STLOC)
    {
        if($item["MATNR"] == "" || $item["MATNR"] == null || empty($item["MATNR"]))
                {
                    exit;
                }
                $price_info = PriceListControl::where('condition_number', $item["KNUMH"])->first();
                $price = 0.00;
                $has_link = true;

                $product = Product::where('sap_product_code',$item["MATNR"])->first();
                if($product == null){
                    $product_id = 0;
                }
                else{
                    $product_id = $product->id;
                }
                
                $base_cur = config('app.BASE_CURRENCY');
                $usd_exc_rates = ExchangeRate::where('baseCurrency',$base_cur)->where('forexCurrency','USD')->first();
                if($usd_exc_rates != null){
                    $rate = round($usd_exc_rates->exchangeRate,4);
                }
                else{
                    $rate = round(1/80,4);
                }
                $price = (float)round($item["KBETR"],2);
                $usd_price = (float)round($price * $rate,2);

                    MainPriceListFromSap::updateOrCreate(
                        [
                           
                            'st_loc' => $STLOC,
                            'plant' => $item['WERKS'],
                            'sap_prod_code'  => $item["MATNR"]
                        ],
                        [
                           'condition_type'  => $item["KSCHL"],
                           'price_list_type'  => $item["PLTYP"],
                           'sales_org'  => $item["VKORG"],
                           'dist_channel'  => $item["VTWEG"],
                           'condition_number' => $item["KNUMH"],                          
                           'valid_from'  => $item["DATAB"],
                           'valid_to'  => $item["DATBI"],
                           'currency'  => $item["WAERK"],  
                           'product_id' => $product_id,                         
                           'qty' => $item['LABST'],
                           'price'  => $price,
                           'price_usd' => $usd_price,
                           'created_by' => auth()->user()->id,
                           'has_cond_number_in_list_I' => $has_link
                        ]
                    );
    }

    /**
     * [ZPRICE] RETURNS
     * "KNUMH" => "0000395528"
     * "KSCHL" => "ZPRO"
     * "PLTYP" => "05"
     * "VKBUR" => "D1"
     * "VTWEG" => "05"
     * "DATAB" => "2020-07-03"
     * "DATBI" => "9999-12-31"
     * "WAERK" => "USD"
     * "MATNR" => "000000000000142077"
     * "KBETR" => "0.0"
     * "MAKTX" => ""
     */
//     public function sales_office_price_list(){
//         $condition_type = "ZPRO";
//         $sap_integration = new SapIntegration();
//         $materials = $sap_integration->Sales_Office_Price_List($condition_type);
//         // $cart_header = $materials["ZPRICE_I"]["item"];
//         // $sort_header = $this->Create_Price_List_Header($cart_header);

//         $cart_prices = $materials["ZPRICE"]["item"];

//         foreach($cart_prices as $item){
//             $price_info = PriceListControl::where('condition_number', $item["KNUMH"])->first();
//             $price = 0.00;
//             $has_link = false;

//             if($price_info != null){
//                 $price = (float)round($price_info->price,2);
//                 $has_link = true;
//             }
//                 SalesOfficePriceList::updateOrCreate(
//                     [
//                         'condition_number' => $item["KNUMH"]
//                     ],
//                     [
//                        'condition_type'  => $item["KSCHL"],
//                        'price_list_type'  => $item["PLTYP"],
//                        'sales_office'  => $item["VKBUR"],
//                        'dist_channel'  => $item["VTWEG"],
//                        'valid_from'  => $item["DATAB"],
//                        'valid_to'  => $item["DATBI"],
//                        'currency'  => $item["WAERK"],
//                        'sap_prod_code'  => $item["MATNR"],
//                        'price'  => $price,
//                        'created_by' => auth()->user()->id,
//                        'has_cond_number_in_list_I' => $has_link
//                     ]
//                 );

              
//             }
// }

    /**
     * optimize ZPRICE_I for sales_officr and cart price to have an effective price structure 
     *  all are returning 90k records
     */

     public function Create_Price_List_Header($array_of_zprices)
     {
        foreach($array_of_zprices as $item){
           
            $check = PriceListControl::where( 'condition_number' , $item["KNUMH"])->first();
            if($check == null){ //temp
                PriceListControl::Create(
                    [
                        'condition_number' => $item["KNUMH"],
                    
                       'cond_type'  => $item["KSCHL"],
                       'price'  => $item["KBETR"],
                       'cond_unit'  => $item["KMEIN"],
                       'va_key'  => $item["VAKEY"],
                        'created_by' => auth()->user()->id
                    ]
                );
            }
            else
            continue;
                // PriceListControl::updateOrCreate(
                //     [
                //         'condition_number' => $item["KNUMH"]
                //     ],
                //     [
                //        'cond_type'  => $item["KSCHL"],
                //        'price'  => $item["KBETR"],
                //        'cond_unit'  => $item["KMEIN"],
                //        'va_key'  => $item["VAKEY"],
                //         'created_by' => auth()->user()->id
                //     ]
                // );
            }
     }

     private function createSapObject(){
        $sapWebService = SapWebservicesconfig::first();
        if($sapWebService == null){
          return redirect()->back()->with('error', "Please configure the webservice parameters.."); 
        }

        $soapWSDL = $sapWebService->url;
        $proxy_ip = $sapWebService->proxy_ip;
        $proxy_port = $sapWebService->proxy_port;
        $proxy_login = $sapWebService->proxy_login;
        $proxy_passkey = $sapWebService->proxy_password;
        $login = $sapWebService->login;
        $password = $sapWebService->password;

          $sap_integration = new SapIntegration($soapWSDL,$proxy_ip,$proxy_port,$proxy_login,$proxy_passkey,$login,$password);
          return $sap_integration;
      
     }

}
