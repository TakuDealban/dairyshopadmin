<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::get();
        return view('categories.index',[
                                        'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:categories,name',
        ]);

      
        $input = $request->all();
        $image = $this->handle_image($request);
        $input['image'] = $image;
        $input['created_by'] = auth()->user()->id;
        Category::create($input);

        return redirect()->route('categories')->with('message','Category has been created succesfully..');
    }

    public function handle_image(Request $request){
        if ($request->hasFile('image')){
            //Get filename with the extension
            $this->validate($request,
         [ 'image' => 'mimes:jpeg,jpg,png' ]);
 
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //Get Just Filename
 
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get Just Ext
            $extension = $request->file('image')->getClientOriginalExtension();
 
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
 
            $request->file('image')->move('uploads/categories', $fileNameToStore);
 
         } else {
             $fileNameToStore = 'default.png';
         }

         return $fileNameToStore;
    }

    public function update_handle_image(Request $request,$old_image){
        if ($request->hasFile('image')){
            //Get filename with the extension
            $this->validate($request,
         [ 'image' => 'mimes:jpeg,jpg,png' ]);
 
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            //Get Just Filename
 
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get Just Ext
            $extension = $request->file('image')->getClientOriginalExtension();
 
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
 
            $request->file('image')->move('uploads/categories', $fileNameToStore);
 
         } else {
             $fileNameToStore = $old_image;
         }

         return $fileNameToStore;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($cat_id)
    {
       $category = Category::findorfail($cat_id);
        return view('categories.edit')->with(compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $category_id)
    {
        $this->validate($request,[
            'name' => 'required|unique:categories,name,'.$category_id,
        ]);
      
        $category = Category::findorfail($category_id);
        $input = $request->all();
        $image = $this->update_handle_image($request,$category->image);
        $input['image'] = $image;
        $input['created_by'] = auth()->user()->id;
        $category->update($input);

        return redirect()->route('categories')->with('message','Category has been updated succesfully..');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
