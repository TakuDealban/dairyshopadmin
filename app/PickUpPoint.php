<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PickUpPoint extends Model
{
    //
    public function storage_products()
    {
      return $this->HasMany(MainPriceListFromSap::class,'st_loc','storage_location');
    }
}
