<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecItems extends Model
{
    protected $table = "recitems";

    public function rec_item_product(){
        return $this->hasOne(Product::class,'id','prodid');
    }

    public function rec_ref_customer(){
        return $this->hasOne(PaymentRequest::class,'request_reference','order_number');
    }
}
