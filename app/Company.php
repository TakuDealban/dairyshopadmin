<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $fillable = ['Name','BPN','VATNo','contact_email','contact_tel','addr_one','addr_two','addr_three','logo','BaseCurrency','Vat_Rate','Paynow_Integration_Key','Paynow_Integration_id','category_image_path','products_image_path','minimum_order_value', 'created_by'];
}
