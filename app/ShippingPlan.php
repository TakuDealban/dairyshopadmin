<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingPlan extends Model
{
    protected $fillable = ['location_to','shipment_charge','shipment_charge_currency','handling_fees','handling_fees_currency','created_by'];

    public function ship_charge_currency(){
        return $this->hasOne(ExchangeRate::class,'id','shipment_charge_currency');
    }

    public function handling_charge_currency(){
        return $this->hasOne(ExchangeRate::class,'id','handling_fees_currency');
    }
}
