<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceListControl extends Model
{
    //
    protected $fillable = ['condition_number','cond_type','price','cond_unit','va_key','created_by'];
}
